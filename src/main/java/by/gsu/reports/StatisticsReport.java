package by.gsu.reports;

import by.gsu.database.ConnectionProvider;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.column.Columns;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.datatype.DataTypes;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import java.sql.SQLException;

import static by.gsu.database.DatabaseConstant.*;

public class StatisticsReport {

    public StatisticsReport() {

    }

    public void fullStatistics(String city) throws SQLException {
        JasperReportBuilder report = DynamicReports.report();
        JasperReportBuilder jasperReportBuilder = report
                .columns(
                        Columns.column("Дата", STAT_TABLE_DATE_COLUMN, DataTypes.stringType()).setHorizontalAlignment(HorizontalAlignment.CENTER),
                        Columns.column("Город", STAT_TABLE_AS_COLUMN_CITY, DataTypes.stringType()),
                        Columns.column("Подстанция", STAT_TABLE_AS_COLUMN_SUBSTATION, DataTypes.stringType()),
                        Columns.column("Температура", STAT_TABLE_TEMP_COLUMN, DataTypes.integerType()).setHorizontalAlignment(HorizontalAlignment.LEFT),
                        Columns.column("Давление", STAT_TABLE_PRESSURE_COLUMN, DataTypes.integerType()).setHorizontalAlignment(HorizontalAlignment.LEFT),
                        Columns.column("Количество осадков", STAT_TABLE_WATER_COLUMN, DataTypes.integerType()).setHorizontalAlignment(HorizontalAlignment.LEFT)
                )
                .title(Components.text("Статистика")
                        .setHorizontalAlignment(HorizontalAlignment.LEFT))
                .pageFooter(Components.pageXofY())
                .setDataSource(EXEC_KEY_WORD + NAME_PROCEDURE_GET_STAT_BY_CITY + " " + city, ConnectionProvider.getConnection());
        try {
            report.show(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void avgStatistics(String city) throws SQLException {
        JasperReportBuilder report = DynamicReports.report();
        JasperReportBuilder jasperReportBuilder = report
                .columns(
                        Columns.column("СредняяТемпература", "СредняяТемпература", DataTypes.doubleType()).setHorizontalAlignment(HorizontalAlignment.CENTER),
                        Columns.column("СредннеДавление", "СредннеДавление", DataTypes.doubleType()).setHorizontalAlignment(HorizontalAlignment.CENTER),
                        Columns.column("СредниеКолВоОсадков", "СредниеКолВоОсадков", DataTypes.doubleType()).setHorizontalAlignment(HorizontalAlignment.CENTER),
                        Columns.column("НазваниеПодстанции", "НазваниеПодстанции", DataTypes.stringType()).setHorizontalAlignment(HorizontalAlignment.CENTER)
                )
                .title(Components.text("Статистика")
                        .setHorizontalAlignment(HorizontalAlignment.LEFT))
                .pageFooter(Components.pageXofY())
                .setDataSource("SELECT * FROM СредниеПоказателиДляГорода('" + city + "')", ConnectionProvider.getConnection());
        try {
            report.show(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
