package by.gsu.reports;

import by.gsu.database.ConnectionProvider;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.column.Columns;
import net.sf.dynamicreports.report.builder.component.Components;
import net.sf.dynamicreports.report.builder.datatype.DataTypes;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import java.sql.SQLException;
import static by.gsu.database.DatabaseConstant.*;


public class EnvironmentReport {

    public EnvironmentReport(String substation) throws SQLException {
        JasperReportBuilder report = DynamicReports.report();
        JasperReportBuilder jasperReportBuilder = report
                .columns(
                        Columns.column("Инвентарный номер", ENVIROMNENT_TABLE_INV_NUMBER_COLUMN, DataTypes.integerType()).setHorizontalAlignment(HorizontalAlignment.CENTER),
                        Columns.column("Серийный номер", ENVIROMNENT_TABLE_SERIAL_NUMBER_COLUMN, DataTypes.stringType()),
                        Columns.column("Тип оборудования", SUBSTATION_TABLE_NAME_COLUMN, DataTypes.stringType()),
                        Columns.column("Количество сбоев", "Сбои", DataTypes.stringType()).setHorizontalAlignment(HorizontalAlignment.LEFT)
                )
                .pageFooter(Components.pageXofY())
                .setDataSource("SELECT * FROM УстановленноеОборудованиеДляПодстанции('" + substation+ "')", ConnectionProvider.getConnection());
        try {
            report.show(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}