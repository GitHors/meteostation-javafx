package by.gsu.models;

public final class Worker {

    private int id;
    private int age;
    private String fio;
    private String gender;

    /**
     *
     */
    public Worker() {
    }

    /**
     *
     * @param id
     * @param age
     * @param fio
     * @param gender
     */
    public Worker(int id, int age, String fio, String gender) {
        this.id = id;
        this.age = age;
        this.fio = fio;
        this.gender = gender;
    }

    /**
     *
     * @param fio
     * @param gender
     * @param age
     */
    public Worker(String fio, String gender, int age) {
        this.fio = fio;
        this.gender = gender;
        this.age = age;
    }

    /**
     *
     * @param fio
     */
    public Worker(String fio) {
        this.fio = fio;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getAge() {
        return age;
    }

    /**
     *
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     *
     * @return
     */
    public String getFio() {
        return fio;
    }

    /**
     *
     * @param fio
     */
    public void setFio(String fio) {
        this.fio = fio;
    }

    /**
     *
     * @return
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

}
