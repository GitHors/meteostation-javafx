package by.gsu.models;

public final class City {

    private int id;
    private String name;
    private int amountOfCitizens;
    private String region;

    /**
     *
     */
    public City() {
    }

    /**
     *
     * @param id
     * @param name
     * @param amountOfCitizens
     * @param region
     */
    public City(int id, String name, int amountOfCitizens, String region) {
        this.id = id;
        this.name = name;
        this.amountOfCitizens = amountOfCitizens;
        this.region = region;
    }

    /**
     *
     * @param name
     * @param amountOfCitizens
     * @param region
     */
    public City(String name, int amountOfCitizens, String region) {
        this.name = name;
        this.amountOfCitizens = amountOfCitizens;
        this.region = region;
    }

    /**
     *
     * @param name
     */
    public City(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public int getAmountOfCitizens() {
        return amountOfCitizens;
    }

    /**
     *
     * @param amountOfCitizens
     */
    public void setAmountOfCitizens(int amountOfCitizens) {
        this.amountOfCitizens = amountOfCitizens;
    }

    /**
     *
     * @return
     */
    public String getRegion() {
        return region;
    }

    /**
     *
     * @param region
     */
    public void setRegion(String region) {
        this.region = region;
    }

}
