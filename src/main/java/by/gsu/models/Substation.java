package by.gsu.models;

public final class Substation {

    private int id;
    private String name;

    private City city;
    private SubstationType substationType;

    public Substation() {
    }

    /**
     *
     * @param id
     * @param name
     * @param city
     * @param substationType
     */
    public Substation(int id, String name, City city, SubstationType substationType) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.substationType = substationType;
    }

    /**
     *
     * @param id
     * @param name
     * @param cityName
     * @param model
     */
    public Substation(int id, String name, String cityName, String model) {
        this.id = id;
        this.name = name;
        this.city = new City(cityName);
        this.substationType = new SubstationType(model);
    }

    /**
     *
     * @param name
     * @param cityName
     * @param model
     */
    public Substation(String name, String cityName, String model) {
        this.name = name;
        this.city = new City(cityName);
        this.substationType = new SubstationType(model);
    }

    /**
     *
     * @param name
     * @param cityName
     */
    public Substation(String name, String cityName) {
        this.name = name;
        this.city = new City(cityName);
    }

    /**
     *
     * @param name
     */
    public Substation(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public City getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(City city) {
        this.city = city;
    }

    /**
     *
     * @return
     */
    public SubstationType getSubstationType() {
        return substationType;
    }

    /**
     *
     * @param substationType
     */
    public void setSubstationType(SubstationType substationType) {
        this.substationType = substationType;
    }
}
