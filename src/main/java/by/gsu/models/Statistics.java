package by.gsu.models;

public final class Statistics {

    private int id;
    private int temperature;
    private int pressure;
    private int precipitation;
    private String date;

    private Substation substation;

    /**
     *
     */
    public Statistics() {
    }

    /**
     *
     * @param id
     * @param temperature
     * @param pressure
     * @param precipitation
     * @param date
     * @param substation
     */
    public Statistics(int id, int temperature, int pressure, int precipitation, String date, Substation substation) {
        this.id = id;
        this.temperature = temperature;
        this.pressure = pressure;
        this.precipitation = precipitation;
        this.date = date;
        this.substation = substation;
    }

    /**
     *
     * @param id
     * @param temperature
     * @param pressure
     * @param precipitation
     * @param date
     * @param cityName
     * @param substName
     */
    public Statistics(int id, int temperature, int pressure, int precipitation, String date, String cityName, String substName) {
        this.id = id;
        this.temperature = temperature;
        this.pressure = pressure;
        this.precipitation = precipitation;
        this.date = date;
        this.substation = new Substation(substName, cityName);
    }

    /**
     *
     * @param temperature
     * @param pressure
     * @param precipitation
     * @param substName
     */
    public Statistics(int temperature, int pressure, int precipitation, String substName) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.precipitation = precipitation;
        this.substation = new Substation(substName);
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getTemperature() {
        return temperature;
    }

    /**
     *
     * @param temperature
     */
    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    /**
     *
     * @return
     */
    public int getPressure() {
        return pressure;
    }

    /**
     *
     * @param pressure
     */
    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    /**
     *
     * @return
     */
    public int getPrecipitation() {
        return precipitation;
    }

    /**
     *
     * @param precipitation
     */
    public void setPrecipitation(int precipitation) {
        this.precipitation = precipitation;
    }

    /**
     *
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public Substation getSubstation() {
        return substation;
    }

    /**
     *
     * @param substation
     */
    public void setSubstation(Substation substation) {
        this.substation = substation;
    }
}
