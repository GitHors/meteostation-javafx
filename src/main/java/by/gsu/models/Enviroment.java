package by.gsu.models;

public final class Enviroment {

    private int id;
    private int inventoryNumber;
    private EnviromentType enviromentType;
    private Substation substation;
    private String serialNumber;

    /**
     *
     */
    public Enviroment() {
    }

    /**
     *
     * @param id
     * @param inventoryNumber
     * @param enviromentType
     * @param substation
     * @param serialNumber
     */
    public Enviroment(int id, int inventoryNumber, EnviromentType enviromentType, Substation substation, String serialNumber) {
        this.id = id;
        this.inventoryNumber = inventoryNumber;
        this.enviromentType = enviromentType;
        this.substation = substation;
        this.serialNumber = serialNumber;
    }

    /**
     *
     * @param id
     * @param inventoryNumber
     * @param serialNumber
     * @param enviromentTypeName
     * @param substationName
     * @param cityName
     */
    public Enviroment(int id, int inventoryNumber, String serialNumber, String enviromentTypeName, String substationName, String cityName) {
        this.id = id;
        this.inventoryNumber = inventoryNumber;
        this.serialNumber = serialNumber;
        this.enviromentType = new EnviromentType(enviromentTypeName);
        this.substation = new Substation(substationName, cityName);
    }

    /**
     *
     * @param inventoryNumber
     * @param serialNumber
     * @param substationName
     * @param environmentTypeName
     */
    public Enviroment(int inventoryNumber, String serialNumber, String substationName, String environmentTypeName) {
        this.inventoryNumber = inventoryNumber;
        this.serialNumber = serialNumber;
        this.substation = new Substation(substationName);
        this.enviromentType = new EnviromentType(environmentTypeName);
    }

    /**
     *
     * @param inventoryNumber
     * @param serialNumber
     * @param substationName
     */
    public Enviroment(int inventoryNumber, String serialNumber, String substationName) {
        this.inventoryNumber = inventoryNumber;
        this.serialNumber = serialNumber;
        this.substation = new Substation(substationName);
    }

    /**
     *
     * @param inventoryNumber
     */
    public Enviroment(int inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }


    /**
     *
     * @return
     */
    public Substation getSubstation() {
        return substation;
    }

    /**
     *
     * @param substation
     */
    public void setSubstation(Substation substation) {
        this.substation = substation;
    }

    /**
     *
     * @return
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     *
     * @param serialNumber
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getInventoryNumber() {
        return inventoryNumber;
    }

    /**
     *
     * @param inventoryNumber
     */
    public void setInventoryNumber(int inventoryNumber) {
        this.inventoryNumber = inventoryNumber;
    }

    /**
     *
     * @return
     */
    public EnviromentType getEnviromentType() {
        return enviromentType;
    }

    /**
     *
     * @param enviromentType
     */
    public void setEnviromentType(EnviromentType enviromentType) {
        this.enviromentType = enviromentType;
    }
}
