package by.gsu.models;

import java.util.Date;

/**
 * Created by denis on 06.04.2017.
 */
public final class Maintenance {

    private int id;
    private String date;

    private Worker worker;
    private Enviroment enviroment;

    /**
     *
     */
    public Maintenance() {
    }

    /**
     *
     * @param id
     * @param date
     * @param worker
     * @param enviroment
     */
    public Maintenance(int id, String date, Worker worker, Enviroment enviroment) {
        this.id = id;
        this.date = date;
        this.worker = worker;
        this.enviroment = enviroment;
    }

    /**
     *
     * @param id
     * @param date
     * @param workerFIO
     * @param enviromentInvNumber
     */
    public Maintenance(int id, String date, String workerFIO, int enviromentInvNumber) {
        this.id = id;
        this.date = date;
        this.worker = new Worker(workerFIO);
        this.enviroment = new Enviroment(enviromentInvNumber);
    }

    /**
     *
     * @param date
     * @param workerFIO
     * @param enviromentInvNumber
     */
    public Maintenance(String date, String workerFIO, int enviromentInvNumber) {
        this.date = date;
        this.worker = new Worker(workerFIO);
        this.enviroment = new Enviroment(enviromentInvNumber);
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public Worker getWorker() {
        return worker;
    }

    /**
     *
     * @param worker
     */
    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    /**
     *
     * @return
     */
    public Enviroment getEnviroment() {
        return enviroment;
    }

    /**
     *
     * @param enviroment
     */
    public void setEnviroment(Enviroment enviroment) {
        this.enviroment = enviroment;
    }
}
