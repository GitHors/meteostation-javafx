package by.gsu.models;

public final class Crash {

    private int id;
    private String date;
    private String time;

    private CrashType crashType;
    private Enviroment enviroment;

    /**
     *
     */
    public Crash() {

    }

    /**
     *
     * @param id
     * @param date
     * @param time
     * @param crashType
     * @param enviroment
     */
    public Crash(int id, String date, String time, CrashType crashType, Enviroment enviroment) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.crashType = crashType;
        this.enviroment = enviroment;
    }

    /**
     *
     * @param id
     * @param date
     * @param time
     * @param crashTypeName
     * @param enviromentInvnumber
     */
    public Crash(int id, String date, String time, String crashTypeName, int enviromentInvnumber) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.crashType = new CrashType(crashTypeName);
        this.enviroment = new Enviroment(enviromentInvnumber);
    }

    /**
     *
     * @param date
     * @param time
     * @param crashTypeName
     * @param enviromentInvnumber
     */
    public Crash(String date, String time, String crashTypeName, int enviromentInvnumber) {
        this.date = date;
        this.time = time;
        this.crashType = new CrashType(crashTypeName);
        this.enviroment = new Enviroment(enviromentInvnumber);
    }


    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     */
    public String getTime() {
        return time;
    }

    /**
     *
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     *
     * @return
     */
    public CrashType getCrashType() {
        return crashType;
    }

    /**
     *
     * @param crashType
     */
    public void setCrashType(CrashType crashType) {
        this.crashType = crashType;
    }

    /**
     *
     * @return
     */
    public Enviroment getEnviroment() {
        return enviroment;
    }

    /**
     *
     * @param enviroment
     */
    public void setEnviroment(Enviroment enviroment) {
        this.enviroment = enviroment;
    }

}
