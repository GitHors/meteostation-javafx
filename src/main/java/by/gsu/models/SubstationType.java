package by.gsu.models;

public final class SubstationType {

    private int id;
    private String model;
    private String material;

    /**
     *
     */
    public SubstationType() {
    }

    /**
     *
     * @param id
     * @param model
     * @param material
     */
    public SubstationType(int id, String model, String material) {
        this.id = id;
        this.model = model;
        this.material = material;
    }

    /**
     *
     * @param model
     * @param material
     */
    public SubstationType(String model, String material) {
        this.model = model;
        this.material = material;
    }

    /**
     *
     * @param model
     */
    public SubstationType(String model) {
        this.model = model;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getModel() {
        return model;
    }

    /**
     *
     * @param model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     *
     * @return
     */
    public String getMaterial() {
        return material;
    }

    /**
     *
     * @param material
     */
    public void setMaterial(String material) {
        this.material = material;
    }

}
