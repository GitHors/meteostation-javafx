package by.gsu.models;

public final class EnviromentType {

    private int id;
    private String name;
    private int size;

    /**
     *
     */
    public EnviromentType() {
    }

    /**
     *
     * @param id
     * @param name
     * @param size
     */
    public EnviromentType(int id, String name, int size) {
        this.id = id;
        this.name = name;
        this.size = size;
    }

    /**
     *
     * @param name
     * @param size
     */
    public EnviromentType(String name, int size) {
        this.name = name;
        this.size = size;
    }

    /**
     *
     * @param name
     */
    public EnviromentType(String name) {
        this.name = name;
    }


    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }
}
