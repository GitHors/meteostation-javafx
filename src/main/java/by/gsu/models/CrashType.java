package by.gsu.models;

public final class CrashType {

    private int id;
    private String description;

    /**
     *
     */
    public CrashType (){

    }

    /**
     *
     * @param id
     * @param description
     */
    public CrashType(int id, String description) {
        this.id = id;
        this.description = description;
    }

    /**
     *
     * @param description
     */
    public CrashType(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}

