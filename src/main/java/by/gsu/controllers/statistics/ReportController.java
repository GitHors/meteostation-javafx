package by.gsu.controllers.statistics;

import by.gsu.actions.city.CityActions;
import by.gsu.actions.statistics.StatisticsActions;
import by.gsu.models.Statistics;
import by.gsu.reports.StatisticsReport;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.SQLException;

public class ReportController {
    public ChoiceBox cityCB;

    public TableView<Statistics> table;

    public TableColumn<Statistics, String> dataStat;
    public TableColumn<Statistics, String> cityName;
    public TableColumn<Statistics, String> tempVal;
    public TableColumn<Statistics, String> pressVal;
    public TableColumn<Statistics, String> waterVal;
    public TableColumn<Statistics, String> substName;
    public Button getWeekButton;

    @FXML
    private void initialize() {
        ObservableList<String> citiesForChoiseBox = new CityActions().getCitiesNames();
        cityCB.setItems(citiesForChoiseBox);
        cityCB.setTooltip(new Tooltip("Выберите населённый пункт"));
        cityCB.valueProperty().addListener((e) -> {render();});
    }

    public void back(ActionEvent actionEvent) {

    }

    public void render() {
        String city = cityCB.getValue().toString();
        ObservableList<Statistics> statistics = new StatisticsActions().get(city);

        dataStat.setCellValueFactory(new PropertyValueFactory<Statistics, String>("date"));
        tempVal.setCellValueFactory(new PropertyValueFactory<Statistics, String>("temperature"));
        pressVal.setCellValueFactory(new PropertyValueFactory<Statistics, String>("pressure"));
        waterVal.setCellValueFactory(new PropertyValueFactory<Statistics, String>("precipitation"));
        cityName.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getSubstation().getCity().getName()));
        substName.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getSubstation().getName()));

        table.setItems(statistics);
    }

    public void showWeekReport(ActionEvent actionEvent) throws SQLException {
        try {
            String city = cityCB.getValue().toString();
            new StatisticsReport().fullStatistics(city);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void showAvgReport(ActionEvent actionEvent) {
        try {
            String city = cityCB.getValue().toString();
            new StatisticsReport().avgStatistics(city);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
