package by.gsu.controllers.statistics;

import by.gsu.actions.statistics.StatisticsActions;
import by.gsu.actions.substation.SubstationActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.Statistics;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;

import static by.gsu.controllers.helpers.ConstrollerHelper.getElementPosition;

public class StatisticsController {

    @FXML
    public TextArea resultArea;
    @FXML
    public TextField search;
    @FXML
    public Button addBtn;

    public TableView<Statistics> table;

    public TableColumn<Statistics, String> dataStat;
    public TableColumn<Statistics, String> cityName;
    public TableColumn<Statistics, String> tempVal;
    public TableColumn<Statistics, String> pressVal;
    public TableColumn<Statistics, String> waterVal;
    public TableColumn<Statistics, String> substName;


    private static Statistics staticStatistics = null;
    public TextField tempValue;
    public TextField pressureValue;
    public TextField waterValue;
    public ChoiceBox substationsCB;

    public Button backButton;

    private static ObservableList<String> sbstForChoiseBox = null;
    private static ObservableList<String> citiesForChoiseBox = null;
    public Button reportBtn;
    public ChoiceBox cityCB;
    public Button updateWrkConfirm;

    public StatisticsController() {

    }

    @FXML
    private void initialize() {
        ObservableList<Statistics> statistics = new StatisticsActions().get();

        dataStat.setCellValueFactory(new PropertyValueFactory<Statistics, String>("date"));
        tempVal.setCellValueFactory(new PropertyValueFactory<Statistics, String>("temperature"));
        pressVal.setCellValueFactory(new PropertyValueFactory<Statistics, String>("pressure"));
        waterVal.setCellValueFactory(new PropertyValueFactory<Statistics, String>("precipitation"));
        cityName.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getSubstation().getCity().getName()));
        substName.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getSubstation().getName()));

        table.setItems(statistics);

        cityCB.setVisible(false);
        sbstForChoiseBox = new SubstationActions().getNames();

        substationsCB.setItems(sbstForChoiseBox);
        substationsCB.setTooltip(new Tooltip("Выберите подстанцию"));
    }

    @FXML
    private void fillTable (ObservableList<Statistics> statistics) throws ClassNotFoundException {
        table.setItems(statistics);
    }


    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<Statistics> statistics = new StatisticsActions().search(search.getText());
            fillTable(statistics);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска");
        }
    }

    public void delete(ActionEvent actionEvent) {
        Statistics statistics  = table.getSelectionModel().getSelectedItem();
        if (statistics == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new StatisticsActions().remove(statistics);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Ошибка удаления");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        Statistics statistics  = table.getSelectionModel().getSelectedItem();
        if (statistics == null){
            resultArea.setText("Выберите запись");
        } else {
            addBtn.setDisable(true);

            tempValue.setText(statistics.getTemperature() + "");
            pressureValue.setText(statistics.getPressure() + "");
            waterValue.setText(statistics.getPrecipitation() + "");
            substationsCB.getSelectionModel().select(getElementPosition(sbstForChoiseBox, statistics.getSubstation().getName()));

            staticStatistics = statistics;
        }
    }

    public void add(ActionEvent actionEvent) {
        if (validate()) {
            try {
                int status = new StatisticsActions().add(getStatistics());
                if (status == 1) {
                    resultArea.setText("Успешно добавлено");
                    renderTable();
                } else {
                    resultArea.setText("Запись не добавлена");
                }
            } catch (Exception e) {
                System.out.println(e);
                resultArea.setText("Ошибка");
            }
        } else {
            resultArea.setText("Заполните информацию корректно");
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if (validate()){
            try{
                int status = new StatisticsActions().update(getStatistics());
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addBtn.setDisable(false);
                    renderTable();
                } else {
                    resultArea.setText("Ошибка");
                }
            } catch (Exception e) {
                resultArea.setText("Ошибка");
            }
        } else {
            resultArea.setText("Заполните информацию корректно");
        }
    }

    @FXML
    private Statistics getStatistics(){
        Statistics statistics = null;
        try {
            int temperature = Integer.valueOf(tempValue.getText());
            int pressure = Integer.valueOf(pressureValue.getText());
            int water = Integer.valueOf(waterValue.getText());
            String substation = substationsCB.getValue().toString();

            statistics = new Statistics(temperature, pressure, water, substation);
            if (staticStatistics != null){
                statistics.setId(staticStatistics.getId());
            }
        } catch (Exception nfe){
            resultArea.setText("error");
        }

        return statistics;
    }

    private void renderTable(){
        ObservableList<Statistics> statistics = new StatisticsActions().get();
        table.setItems(statistics);
    }

    public boolean isValidNumber(String s){
        String regex="([-]){0,1}([0-9])+";
        return s.matches(regex);
    }

    public boolean validate() {
        boolean res = false;
        if(!isValidNumber(tempValue.getText().trim()) ||
                !isValidNumber(pressureValue.getText().trim()) ||
                !isValidNumber(waterValue.getText().trim())){
        }else{
            res = true;
        }

        return res;
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }

    public void reportStart(ActionEvent actionEvent) {
        Stage primaryStage = new Stage();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/statistics/reports/ReportsView.fxml"));
            primaryStage.setTitle("Отчёты");
            primaryStage.setScene(new Scene(root, 1000, 800));
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
