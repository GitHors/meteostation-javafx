package by.gsu.controllers.worker;

import by.gsu.actions.worker.WorkerActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.City;
import by.gsu.models.Worker;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

public class WorkerController {

    public Button deleteWrkBtn;
    public Button searchWrkBtn;
    public Button updateWrkBtn;
    public Button addWrkBtn;
    public Button searchWrksBtn;
    public TextField wrkIdText;
    public TextField ageText;
    public TextField fioText;
    public ComboBox genderCB;
    public Button updateWrkConfirm;
    public TextArea resultArea;
    public Button backButton;

    @FXML
    TableView<Worker> tableWorkers;

    @FXML
    private TableColumn<City, String> fioColumn;

    @FXML
    private TableColumn<City, String> genderColumn;

    @FXML
    private TableColumn<City, Integer> ageColumn;

    private static Worker workerStat;

    @FXML
    private void initialize() {
        ObservableList<Worker> workers = new WorkerActions().get();

        fioColumn.setCellValueFactory(new PropertyValueFactory<City, String>("fio"));
        genderColumn.setCellValueFactory(new PropertyValueFactory<City, String>("gender"));
        ageColumn.setCellValueFactory(new PropertyValueFactory<City, Integer>("age"));

        tableWorkers.setItems(workers);
    }

    public void search(ActionEvent actionEvent) {

        try {
            ObservableList<Worker> workers = new WorkerActions().search(wrkIdText.getText());
            fillTable(workers);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска");
        }
    }

    @FXML
    private void fillTable (ObservableList<Worker> workers) throws ClassNotFoundException {
        tableWorkers.setItems(workers);
    }

    public void add(ActionEvent actionEvent) {
        try {
            String fio = fioText.getText();
            String gender = genderCB.getValue().toString();

            if (fio.isEmpty() || gender.isEmpty()){
                resultArea.setText("Заполните информацию");
            } else {
                int age = Integer.parseInt(ageText.getText());

                Worker worker = new Worker(fio, gender, age);
                int status = new WorkerActions().add(worker);
                if (status == 1){
                    resultArea.setText("Успешно добавлено");
                    renderTable();
                } else {
                    resultArea.setText("Запись не уникальна");
                }
            }
        } catch (NumberFormatException nfe){
            resultArea.setText("Введите корректный возраст");
        }
        catch (Exception e){
            resultArea.setText("Ошибка");
        }
    }

    private void renderTable(){
        ObservableList<Worker> workers = new WorkerActions().get();
        tableWorkers.setItems(workers);
    }

    public void delete(ActionEvent actionEvent) {
        Worker worker = tableWorkers.getSelectionModel().getSelectedItem();
        if (worker == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new WorkerActions().remove(worker);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Запись используется в других таблицах");
            }
        }

    }

    public void update(ActionEvent actionEvent) {
        Worker worker = tableWorkers.getSelectionModel().getSelectedItem();
        if (worker == null){
            resultArea.setText("Выберите запись");
        } else {
            addWrkBtn.setDisable(true);
            fioText.setText(worker.getFio());
            ageText.setText(String.valueOf(worker.getAge()));
            genderCB.setValue("Мужской");
            workerStat = worker;
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        try {
            String fio = fioText.getText();
            String gender = genderCB.getValue().toString();

            if (fio.isEmpty() || gender.isEmpty()) {
                resultArea.setText("Заполните информацию");
            } else {
                int age = Integer.parseInt(ageText.getText());

                workerStat.setFio(fio);
                workerStat.setGender(gender);
                workerStat.setAge(age);

                int status = new WorkerActions().update(workerStat);
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addWrkBtn.setDisable(false);
                    renderTable();
                } else {
                    resultArea.setText("Запись не уникальна");
                }
            }

        } catch (NumberFormatException nfe) {
            resultArea.setText("Введите корректный возраст");
        } catch (Exception e) {
            resultArea.setText("Ошибка");
        }
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }
}
