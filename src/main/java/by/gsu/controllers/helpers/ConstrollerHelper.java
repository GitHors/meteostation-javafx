package by.gsu.controllers.helpers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class ConstrollerHelper {

    public ConstrollerHelper() {
    }

    public static int getElementPosition(ObservableList<String> list, String value){
        int index = -1;
        for (int i = 0; i < list.size(); i++){
            if (list.get(i).equals(value)){
                return i;
            }
        }
        return index;
    }

    public void openMain(ActionEvent actionEvent, Button backButton) {
        openWindow(actionEvent,"/views/main/Main.fxml", "Метеостанция");
        closeFrom(backButton);
    }

    public void openWindow(ActionEvent actionEvent, String viewName, String title){
        Stage primaryStage = new Stage();
        try {
            Parent root = FXMLLoader.load(getClass().getResource(viewName));
            primaryStage.setTitle(title);
            primaryStage.setScene(new Scene(root, 1000, 800));
            primaryStage.show();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeFrom(Button backButton){
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
    }
}
