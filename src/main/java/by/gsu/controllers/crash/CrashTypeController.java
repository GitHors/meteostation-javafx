package by.gsu.controllers.crash;

import by.gsu.actions.crash.CrashTypeActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.CrashType;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

public class CrashTypeController {

    public TextArea resultArea;
    public TextField search;
    public Button addBtn;
    public TextField descriptionName;
    public Button backButton;
    public Button updateWrkConfirm;

    @FXML
    TableView<CrashType> table;
    @FXML
    private TableColumn<CrashType, String> crashType;

    private static CrashType staticCrashType = null;

    public CrashTypeController() {

    }

    @FXML
    private void initialize() {
        ObservableList<CrashType> types = new CrashTypeActions().get();
        crashType.setCellValueFactory(new PropertyValueFactory<CrashType, String>("description"));
        updateWrkConfirm.setDisable(true);
        table.setItems(types);
    }
    @FXML
    private void fillTable (ObservableList<CrashType> crashTypes) throws ClassNotFoundException {
        table.setItems(crashTypes);
    }

    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<CrashType> crashTypes = new CrashTypeActions().search(search.getText());
            fillTable(crashTypes);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска" + e);
        }
    }

    public void delete(ActionEvent actionEvent) {
        CrashType crashType  = table.getSelectionModel().getSelectedItem();
        if (crashType == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new CrashTypeActions().remove(crashType);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Ошибка удаления, запись используется в других таблицах");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        CrashType crashType  = table.getSelectionModel().getSelectedItem();
        if (crashType == null){
            resultArea.setText("Выберите запись");
        } else {
            updateWrkConfirm.setDisable(false);
            addBtn.setDisable(true);
            descriptionName.setText(crashType.getDescription());
            staticCrashType = crashType;
        }
    }

    public void add(ActionEvent actionEvent) {
        if (!descriptionName.getText().trim().isEmpty()) {
            try {
                int status = new CrashTypeActions().add(getCrashType());
                if (status == 1) {

                    resultArea.setText("Запись добавлена");
                    renderTable();
                    clearFields();
                    updateWrkConfirm.setDisable(true);
                } else {
                    resultArea.setText("Введите уникальное значение");
                }
            } catch (Exception e) {
                System.out.println(e);
                resultArea.setText("ошибка");
            }
        } else {
            resultArea.setText("Введите информацию!");
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if (!descriptionName.getText().trim().isEmpty()){
            try{
                int status = new CrashTypeActions().update(getCrashType());
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addBtn.setDisable(false);
                    renderTable();
                    clearFields();
                    updateWrkConfirm.setDisable(true);
                } else {
                   resultArea.setText("Ошибка");
                }
            } catch (Exception e) {
                resultArea.setText("ошибка");
            }
        } else {
            resultArea.setText("Введите информацию!");
        }
    }

    @FXML
    private CrashType getCrashType(){
        CrashType crashType = null;
        try {
            String description = descriptionName.getText();
            crashType = new CrashType(description);
            if (staticCrashType != null){
                crashType.setId(staticCrashType.getId());
            }
        } catch (Exception nfe){
            resultArea.setText("error");
        }

        return crashType;
    }

    private void renderTable(){
        ObservableList<CrashType> crashTypes = new CrashTypeActions().get();
        table.setItems(crashTypes);
    }

    private void clearFields(){
        descriptionName.setText("");
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }
}
