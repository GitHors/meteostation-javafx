package by.gsu.controllers.crash;

import by.gsu.actions.crash.CrashActions;
import by.gsu.actions.crash.CrashTypeActions;
import by.gsu.actions.environment.EnvironmentActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.Crash;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

import static by.gsu.controllers.helpers.ConstrollerHelper.getElementPosition;

public class CrashController {

    public TextArea resultArea;
    public TextField search;
    public Button addBtn;

    public TableColumn<Crash, String> typeCrash;
    public TableColumn<Crash, String> dateCol;
    public TableColumn<Crash, String> timeCol;
    public TableColumn<Crash, String> invNum;

    public ChoiceBox envCB;
    public ChoiceBox crashTypeCB;
    public TextField dateInput;

    public Button backButton;
    public Button updateWrkConfirm;
    public DatePicker dateI;
    public TextField timeInput;

    @FXML
    TableView<Crash> table;

    private static ObservableList<String> crashTypesForChoiseBox = null;
    private static ObservableList<String> envForChoiseBox = null;

    private static Crash crashStatic = null;

    public CrashController() {
    }

    @FXML
    private void initialize() {
        ObservableList<Crash> crashes = new CrashActions().get();

        dateCol.setCellValueFactory(new PropertyValueFactory<Crash, String>("date"));
        timeCol.setCellValueFactory(new PropertyValueFactory<Crash, String>("time"));
        invNum.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getEnviroment().getInventoryNumber() + ""));
        typeCrash.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getCrashType().getDescription()));

        crashTypesForChoiseBox = new CrashTypeActions().getTypes();
        envForChoiseBox = new EnvironmentActions().getInventoryNumbers();

        crashTypeCB.setItems(crashTypesForChoiseBox);
        crashTypeCB.setTooltip(new Tooltip("Выберите тип сбоя"));

        updateWrkConfirm.setDisable(true);

        envCB.setItems(envForChoiseBox);
        envCB.setTooltip(new Tooltip("Выберите оборудование"));

        renderDate();

        table.setItems(crashes);
    }

    @FXML
    private void fillTable (ObservableList<Crash> enviromentTypes) throws ClassNotFoundException {
        table.setItems(enviromentTypes);
    }

    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<Crash> crashes = new CrashActions().search(search.getText());
            fillTable(crashes);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Error occurred while getting employee information from DB.\n" + e);
        }
    }

    public void delete(ActionEvent actionEvent) {
        Crash crash  = table.getSelectionModel().getSelectedItem();
        if (crash == null){
            resultArea.setText("выберите запись");
        } else {
            int status = new CrashActions().remove(crash);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Ошибка");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        Crash crash  = table.getSelectionModel().getSelectedItem();
        if (crash == null){
            resultArea.setText("выберите запись");
        } else {
            updateWrkConfirm.setDisable(false);
            addBtn.setDisable(true);
            dateI.setValue(LocalDate.parse(crash.getDate()));
            timeInput.setText(crash.getTime());
            crashTypeCB.getSelectionModel().select(getElementPosition(crashTypesForChoiseBox, crash.getCrashType().getDescription()));
            envCB.getSelectionModel().select(getElementPosition(envForChoiseBox, crash.getEnviroment().getInventoryNumber() + ""));

            crashStatic = crash;
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if (timeInput.getText().trim().isEmpty() ||
                crashTypeCB.getSelectionModel().getSelectedIndex() == -1 ||
                envCB.getSelectionModel().getSelectedIndex() == -1){
            resultArea.setText("Заполните всю информацию");
        } else {
            if (isValidTime(timeInput.getText())){
                try {
                    Crash crash = getCrash();
                    int status = new CrashActions().update(crash);
                    if (status == 1) {
                        resultArea.setText("Изменено");
                        addBtn.setDisable(false);
                        renderTable();
                        updateWrkConfirm.setDisable(true);
                        clearFields();
                    } else {
                        resultArea.setText("Ошибка");
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    resultArea.setText("Ошибка");
                }
            } else {
                resultArea.setText("Неверный формат времени");
            }
        }
    }

    public void add(ActionEvent actionEvent) {
        if (timeInput.getText().trim().isEmpty() ||
                crashTypeCB.getSelectionModel().getSelectedIndex() == -1 ||
                envCB.getSelectionModel().getSelectedIndex() == -1){
            resultArea.setText("Заполните всю информацию");
        } else {
            if (isValidTime(timeInput.getText())) {
                try {
                    Crash enviroment = getCrash();
                    int status = new CrashActions().add(enviroment);
                    if (status == 1) {
                        resultArea.setText("Успешно добавлено");
                        renderTable();
                    } else {
                        resultArea.setText("Не добавлено");
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    resultArea.setText("Ошибка");
                }
            } else {
                resultArea.setText("Неверный формат времени");
            }
        }
    }

    @FXML
    private Crash getCrash(){
        Crash crash = null;
        try {
            String dateValue = dateI.getValue().toString();
            String timeValue = timeInput.getText();
            String crashType = crashTypeCB.getValue().toString();
            int environment = Integer.valueOf(envCB.getValue().toString());
            crash = new Crash(dateValue, timeValue, crashType, environment);

            if (crashStatic != null){
                crash.setId(crashStatic.getId());
            }
        } catch (NumberFormatException nfe){
            resultArea.setText("введите корректное число");
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        return crash;
    }

    private void renderTable(){
        ObservableList<Crash> crashes = new CrashActions().get();
        table.setItems(crashes);
    }

    public boolean isValidTime(String s){
        return Pattern.compile("(([0-9]){2}:){2}([0-9]){2}").matcher(s.trim()).matches();
    }

    private void clearFields(){
        timeInput.setText("");
        renderDate();
        envCB.getSelectionModel().select(-1);
        crashTypeCB.getSelectionModel().select(-1);
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }

    private void renderDate(){
        dateI.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }
}
