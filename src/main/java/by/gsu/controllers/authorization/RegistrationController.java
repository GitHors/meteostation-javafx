package by.gsu.controllers.authorization;

import by.gsu.actions.user.UserActions;
import by.gsu.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class RegistrationController {

    public TextField loginValue;
    public TextField passwordValue;
    public Label errorText;

    public void registration(ActionEvent actionEvent) {
        String login = loginValue.getText();
        String password = passwordValue.getText();

        if (login.isEmpty()){
            errorText.setText("Введите логин");
        } else if (password.isEmpty()){
            errorText.setText("Введите пароль");
        } else {
            User user = new User(login, password);
            if (new UserActions().add(user) == 1){
                Stage primaryStage = new Stage();
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("/views/authorization/Login.fxml"));
                    primaryStage.setTitle("Логин");
                    primaryStage.setScene(new Scene(root, 1000, 800));
                    primaryStage.show();

                    closeFrom();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                errorText.setText("Ошибка регистрации");
            }
        }
    }

    private void closeFrom(){
        Stage stage = (Stage) loginValue.getScene().getWindow();
        stage.close();
    }
}
