package by.gsu.controllers.authorization;

import by.gsu.actions.user.UserActions;
import by.gsu.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {
    public TextField loginValue;
    public TextField passwordValue;
    public Label errorText;

    public void login(ActionEvent actionEvent) {
        String login = loginValue.getText().trim();
        String password = passwordValue.getText().trim();

        if (login.isEmpty()){
            errorText.setText("Введите логин");
        } else if (password.isEmpty()){
            errorText.setText("Введите пароль");
        } else {
            if (new UserActions().isUserInfoCorrect(new User(login, password))){
                Stage primaryStage = new Stage();
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("/views/main/Main.fxml"));
                    primaryStage.setTitle("Метеостанция");
                    primaryStage.setScene(new Scene(root, 1000, 800));
                    primaryStage.show();

                    closeFrom();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                errorText.setText("Не правильный логин или пароль");
            }
        }
    }

    public void registration(ActionEvent actionEvent) {
        Stage primaryStage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/views/authorization/Registration.fxml"));
            primaryStage.setTitle("Регистрация");
            primaryStage.setScene(new Scene(root, 1000, 800));
            primaryStage.show();

            closeFrom();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeFrom(){
        Stage stage = (Stage) loginValue.getScene().getWindow();
        stage.close();
    }
}
