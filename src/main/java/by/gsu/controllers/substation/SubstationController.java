package by.gsu.controllers.substation;

import by.gsu.actions.city.CityActions;
import by.gsu.actions.substation.SubstationActions;
import by.gsu.actions.substation.SubstationTypesActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.Substation;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import static by.gsu.controllers.helpers.ConstrollerHelper.*;

public class SubstationController {

    @FXML
    public TextArea resultArea;
    @FXML
    public TextField search;
    @FXML
    public Button addBtn;
    @FXML
    public ChoiceBox modelsCB;
    @FXML
    public ChoiceBox citiesCB;
    @FXML
    public TextField nazvName;

    public Button backButton;
    public Button updateWrkConfirm;

    @FXML
    TableView<Substation> table;
    @FXML
    private TableColumn<Substation, String> nameSubst;
    @FXML
    private TableColumn<Substation, String> typeName;
    @FXML
    private TableColumn<Substation, String> cityName;

    private static Substation staticSubstation = null;
    private static ObservableList<String> sbstTypesForChoiseBox = null;
    private static ObservableList<String> citiesForChoiseBox = null;
    @FXML
    private void initialize() {
        ObservableList<Substation> substations = new SubstationActions().get();

        nameSubst.setCellValueFactory(new PropertyValueFactory<Substation, String>("name"));
        typeName.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getSubstationType().getModel()));
        cityName.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getCity().getName()));

        table.setItems(substations);


        sbstTypesForChoiseBox = new SubstationTypesActions().getModels();
        citiesForChoiseBox = new CityActions().getCitiesNames();

        modelsCB.setItems(sbstTypesForChoiseBox);
        citiesCB.setItems(citiesForChoiseBox);

        modelsCB.setTooltip(new Tooltip("Выберите модель"));
        citiesCB.setTooltip(new Tooltip("Выберите город"));
    }

    @FXML
    private void fillTable (ObservableList<Substation> substation) throws ClassNotFoundException {
        table.setItems(substation);
    }


    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<Substation> substation = new SubstationActions().search(search.getText());
            fillTable(substation);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска");
        }
    }

    public void delete(ActionEvent actionEvent) {
        Substation substation  = table.getSelectionModel().getSelectedItem();
        if (substation == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new SubstationActions().remove(substation);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Запись используется в другой таблице");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        Substation substation  = table.getSelectionModel().getSelectedItem();
        if (substation == null){
            resultArea.setText("Выберите запись");
        } else {
            addBtn.setDisable(true);

            nazvName.setText(substation.getName());
            modelsCB.getSelectionModel().select(getElementPosition(sbstTypesForChoiseBox, substation.getSubstationType().getModel()));
            citiesCB.getSelectionModel().select(getElementPosition(citiesForChoiseBox, substation.getCity().getName()));

            staticSubstation = substation;
        }
    }

    public void add(ActionEvent actionEvent) {
        if (citiesCB.getSelectionModel().getSelectedIndex() == -1 ||
                modelsCB.getSelectionModel().getSelectedIndex() == -1 ||
                nazvName.getText().trim().isEmpty()){
            resultArea.setText("Заполните всю информацию");
        } else {
            try {
                int status = new SubstationActions().add(getSubstation());
                if (status == 1) {
                    resultArea.setText("Успешно добавлено");
                    renderTable();
                } else {
                    resultArea.setText("Запись не уникальна");
                }
            } catch (Exception e) {
                System.out.println(e);
                resultArea.setText("Ошибка");
            }
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if (citiesCB.getSelectionModel().getSelectedIndex() == -1 ||
                modelsCB.getSelectionModel().getSelectedIndex() == -1 ||
                nazvName.getText().trim().isEmpty()){
            resultArea.setText("Заполните всю информацию");
        } else {
            try{
                int status = new SubstationActions().update(getSubstation());
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addBtn.setDisable(false);
                    renderTable();
                } else {
                    resultArea.setText("Ошибка уникальности");
                }
            } catch (Exception e) {
                resultArea.setText("Ошибка");
            }
        }

    }

    @FXML
    private Substation getSubstation(){
        Substation substation = null;
        try {
            String nazvNameParam = nazvName.getText();
            String city = citiesCB.getValue().toString();
            String type = modelsCB.getValue().toString();
            substation = new Substation(nazvNameParam, city, type);
            if (staticSubstation != null){
                substation.setId(staticSubstation.getId());
            }
        } catch (Exception nfe){
            resultArea.setText("error");
        }

        return substation;
    }

    private void renderTable(){
        ObservableList<Substation> substation = new SubstationActions().get();
        table.setItems(substation);
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }

}
