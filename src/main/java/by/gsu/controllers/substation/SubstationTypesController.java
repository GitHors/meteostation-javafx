package by.gsu.controllers.substation;

import by.gsu.actions.substation.SubstationTypesActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.SubstationType;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

public class SubstationTypesController {

    public TextArea resultArea;
    public TextField search;
    public Button addBtn;
    public TextField descriptionName;
    public TextField modelName;
    public TextField materialName;
    public Button backButton;
    public Button updateWrkConfirm;

    @FXML
    TableView<SubstationType> table;
    @FXML
    private TableColumn<SubstationType, String> modelSubStColumn;
    @FXML
    private TableColumn<SubstationType, String> materialSubStColumn;

    private static SubstationType staticSubstationType = null;

    public SubstationTypesController() {
    }

    @FXML
    private void initialize() {
        ObservableList<SubstationType> substationTypes = new SubstationTypesActions().get();

        modelSubStColumn.setCellValueFactory(new PropertyValueFactory<SubstationType, String>("model"));
        materialSubStColumn.setCellValueFactory(new PropertyValueFactory<SubstationType, String>("material"));

        updateWrkConfirm.setDisable(true);
        table.setItems(substationTypes);
    }

    @FXML
    private void fillTable (ObservableList<SubstationType> substationTypes) throws ClassNotFoundException {
        table.setItems(substationTypes);
    }


    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<SubstationType> substationTypes = new SubstationTypesActions().search(search.getText());
            fillTable(substationTypes);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска");
        }
    }

    public void delete(ActionEvent actionEvent) {
        SubstationType substationType  = table.getSelectionModel().getSelectedItem();
        if (substationType == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new SubstationTypesActions().remove(substationType);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Запись используется в других таблицах");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        SubstationType substationType  = table.getSelectionModel().getSelectedItem();
        if (substationType == null){
            resultArea.setText("выберите запись");
        } else {
            addBtn.setDisable(true);
            updateWrkConfirm.setDisable(false);
            modelName.setText(substationType.getModel());
            materialName.setText(substationType.getMaterial());
            staticSubstationType = substationType;
        }
    }

    public void add(ActionEvent actionEvent) {
        if (!modelName.getText().trim().isEmpty() ||
                !materialName.getText().trim().isEmpty()) {
            try {
                int status = new SubstationTypesActions().add(getSubstationType());
                if (status == 1) {
                    resultArea.setText("Успешно добавлено");
                    renderTable();
                    clearFields();
                } else {
                    resultArea.setText("Запись не уникальна");
                }
            } catch (Exception e) {
                System.out.println(e);
                resultArea.setText("Ошибка");
            }
        } else {
            resultArea.setText("Заполните все поля");
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if (!modelName.getText().trim().isEmpty() ||
                !materialName.getText().trim().isEmpty()){
            try{
                int status = new SubstationTypesActions().update(getSubstationType());
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addBtn.setDisable(false);
                    renderTable();
                    clearFields();
                } else {
                    resultArea.setText("Запись не уникальна");
                }
            } catch (Exception e) {
                resultArea.setText("Ошибка");
            }
        } else {
            resultArea.setText("Заполните все поля");
        }
    }

    @FXML
    private SubstationType getSubstationType(){
        SubstationType substationType = null;
        try {
            String model = modelName.getText();
            String material = materialName.getText();
            substationType = new SubstationType(model, material);
            if (staticSubstationType != null){
                substationType.setId(staticSubstationType.getId());
            }
        } catch (Exception nfe){
            resultArea.setText("Ошибка");
        }

        return substationType;
    }

    private void renderTable(){
        ObservableList<SubstationType> substationTypes = new SubstationTypesActions().get();
        table.setItems(substationTypes);
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }

    private void clearFields(){
        modelName.setText("");
        materialName.setText("");
    }

}
