package by.gsu.controllers.maintence;

import by.gsu.actions.environment.EnvironmentActions;
import by.gsu.actions.maintence.MaintenceActions;
import by.gsu.actions.worker.WorkerActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.Maintenance;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static by.gsu.controllers.helpers.ConstrollerHelper.getElementPosition;

public class MaintenceController {

    public TextArea resultArea;
    public TextField search;
    public Button addBtn;

    public TableColumn<Maintenance, String> dateCol;
    public TableColumn<Maintenance, String> fioCol;
    public TableColumn<Maintenance, String> invNum;

    public ChoiceBox workerCB;
    public ChoiceBox envCB;
    public TextField dateValue;
    public Button backButton;
    public DatePicker dateI;

    @FXML
    TableView<Maintenance> table;

    private static ObservableList<String> envForChoiseBox = null;
    private static ObservableList<String> workersForChoiseBox = null;

    private static Maintenance maintenanceStatic = null;


    @FXML
    private void initialize() {
        ObservableList<Maintenance> enviromentTypes = new MaintenceActions().get();

        dateCol.setCellValueFactory(new PropertyValueFactory<Maintenance, String>("date"));
        fioCol.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getWorker().getFio()));
        invNum.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getEnviroment().getInventoryNumber() + ""));

        envForChoiseBox = new EnvironmentActions().getInventoryNumbers();
        workersForChoiseBox = new WorkerActions().getWorkers();

        envCB.setItems(envForChoiseBox);
        envCB.setTooltip(new Tooltip("Выберите обрудование"));

        workerCB.setItems(workersForChoiseBox);
        workerCB.setTooltip(new Tooltip("Выберите сотрудника"));

        // заполняем таблицу данными
        table.setItems(enviromentTypes);
    }

    @FXML
    private void fillTable (ObservableList<Maintenance> maintenances) throws ClassNotFoundException {
        table.setItems(maintenances);
    }

    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<Maintenance> maintenances = new MaintenceActions().search(search.getText());
            fillTable(maintenances);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска");
        }
    }

    public void delete(ActionEvent actionEvent) {
        Maintenance maintenance  = table.getSelectionModel().getSelectedItem();
        if (maintenance == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new MaintenceActions().remove(maintenance);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Ошибка удаления");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        Maintenance maintenance  = table.getSelectionModel().getSelectedItem();
        if (maintenance == null){
            resultArea.setText("Выберите запись");
        } else {
            addBtn.setDisable(true);
            dateI.setValue(LocalDate.parse(maintenance.getDate()));
            envCB.getSelectionModel().select(getElementPosition(envForChoiseBox, maintenance.getEnviroment().getInventoryNumber() + ""));
            workerCB.getSelectionModel().select(getElementPosition(workersForChoiseBox, maintenance.getWorker().getFio()));

            maintenanceStatic = maintenance;
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if ( workerCB.getSelectionModel().getSelectedIndex() == -1 ||
                envCB.getSelectionModel().getSelectedIndex() == -1){
            resultArea.setText("Заполните всю информацию");
        } else {
            try {
                int status = new MaintenceActions().update(getMaintenance());
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addBtn.setDisable(false);
                    renderTable();
                } else {
                    resultArea.setText("Ошибка");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                resultArea.setText("Ошибка");
            }
        }
    }

    public void add(ActionEvent actionEvent) {
        if (    workerCB.getSelectionModel().getSelectedIndex() == -1 ||
                envCB.getSelectionModel().getSelectedIndex() == -1){
            resultArea.setText("Заполните всю информацию");
        } else {
            try {
                Maintenance enviroment = getMaintenance();
                int status = new MaintenceActions().add(enviroment);
                if (status == 1) {
                    resultArea.setText("Успешно добавлено");
                    renderTable();
                } else {
                    resultArea.setText("Ошибка добавления");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                resultArea.setText("Ошибка");
            }
        }
    }

    @FXML
    private Maintenance getMaintenance(){
        Maintenance maintenance = null;
        try {
            String dataValueInput = dateI.getValue().toString();
            int invNumberValue = Integer.valueOf(envCB.getValue().toString());
            String workerValue = workerCB.getValue().toString();

            maintenance = new Maintenance(dataValueInput, workerValue, invNumberValue);

            if (maintenanceStatic != null){
                maintenance.setId(maintenanceStatic.getId());
            }
        } catch (NumberFormatException nfe){
            resultArea.setText("Введите корректное число");
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        return maintenance;
    }

    private void renderTable(){
        ObservableList<Maintenance> maintenances = new MaintenceActions().get();
        table.setItems(maintenances);
    }

    private void renderDate(){
        dateI.setConverter(new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }
}
