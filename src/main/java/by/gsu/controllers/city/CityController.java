package by.gsu.controllers.city;

import by.gsu.actions.city.CityActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.City;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;


public class CityController {

    public TextArea resultArea;
    public TextField cityName;
    public TextField citizens;
    public TextField search;
    public TextField regionName;
    public Button addBtn;
    public Button backButton;
    public Button updateWrkConfirm;

    @FXML
    TableView<City> table;
    @FXML
    private TableColumn<City, String> cityNameColumn;
    @FXML
    private TableColumn<City, String> regionColumn;
    @FXML
    private TableColumn<City, Integer> citizensColomn;

    private static City cityStatic;

    public CityController(){
    }

    @FXML
    private void initialize() {
        ObservableList<City> cities = new CityActions().get();

        cityNameColumn.setCellValueFactory(new PropertyValueFactory<City, String>("name"));
        citizensColomn.setCellValueFactory(new PropertyValueFactory<City, Integer>("amountOfCitizens"));
        regionColumn.setCellValueFactory(new PropertyValueFactory<City, String>("region"));

        updateWrkConfirm.setDisable(true);

        table.setItems(cities);
    }

    @FXML
    private void fillTable (ObservableList<City> cities) throws ClassNotFoundException {
        table.setItems(cities);
    }

    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<City> cities = new CityActions().search(search.getText());
            fillTable(cities);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска" + e);
        }
    }

    public void delete(ActionEvent actionEvent) {
        City city  = table.getSelectionModel().getSelectedItem();
        if (city == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new CityActions().remove(city);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Ошибка удаления, запись используется в других таблицах");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        City city = table.getSelectionModel().getSelectedItem();
        if (city == null){
            resultArea.setText("Выберите запись");
        } else {
            updateWrkConfirm.setDisable(false);
            addBtn.setDisable(true);
            cityName.setText(city.getName());
            regionName.setText(city.getRegion());
            citizens.setText(city.getAmountOfCitizens() + "");
            cityStatic = city;
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if (regionName.getText().trim().isEmpty() || citizens.getText().trim().isEmpty() || cityName.getText().trim().isEmpty()){
            resultArea.setText("Заполните все поля для добавления");
        } else {
            try{
                int status = new CityActions().update(getCity());
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addBtn.setDisable(false);
                    renderTable();
                    clearFields();
                    updateWrkConfirm.setDisable(true);
                } else {
                    resultArea.setText("Ошибка");
                }
            } catch (Exception e) {
                resultArea.setText("Ошибка");
            }
        }
    }

    public void add(ActionEvent actionEvent) {
        if (regionName.getText().trim().isEmpty() || citizens.getText().trim().isEmpty() || cityName.getText().trim().isEmpty()){
            resultArea.setText("Заполните все поля для добавления");
        } else {
            try {
                int status = new CityActions().add(getCity());
                if (status == 1){
                    resultArea.setText("Запись успешно добавлена");
                    renderTable();
                    clearFields();
                } else {
                    resultArea.setText("Ошибка");
                }
            }
            catch (Exception e){
                System.out.println(e);
                resultArea.setText("Ошибка");
            }
        }
    }

    @FXML
    private City getCity(){
        City city = null;
        try {
            String name = cityName.getText();
            String region = regionName.getText();
            int amountOfPeople = Integer.valueOf(citizens.getText());
            city = new City(name, amountOfPeople, region);
            if (cityStatic != null){
                city.setId(cityStatic.getId());
            }
        } catch (NumberFormatException nfe){
        }
        return city;
    }

    private void renderTable(){
        ObservableList<City> workers = new CityActions().get();
        table.setItems(workers);
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }

    public boolean isValidNumber(String s){
        String regex="([0-9])+";
        return s.matches(regex);
    }

    public void validate(KeyEvent keyEvent) {
        if(isValidNumber(citizens.getText())){
            addBtn.setDisable(false);
            if (!updateWrkConfirm.isDisabled()) {
                updateWrkConfirm.setDisable(false);
            }
        }else{
            resultArea.setText("Введите число");
            addBtn.setDisable(true);
            updateWrkConfirm.setDisable(true);
        }
    }

    private void clearFields(){
        regionName.setText("");
        citizens.setText("");
        cityName.setText("");
    }
}
