package by.gsu.controllers.environment;

import by.gsu.actions.environment.EnvironmentActions;
import by.gsu.actions.environment.EnvironmentTypesActions;
import by.gsu.actions.substation.SubstationActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.Enviroment;
import by.gsu.reports.EnvironmentReport;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import static by.gsu.controllers.helpers.ConstrollerHelper.getElementPosition;

/**
 * Created by denis on 02.05.2017.
 */
public class EnvironmentController {

    @FXML
    public TextArea resultArea;
    @FXML
    public TextField search;
    public Button addBtn;

    public TableColumn<Enviroment, String> cityCol;
    public TableColumn<Enviroment, String> substCol;
    public TableColumn<Enviroment, String> typeCol;
    public TableColumn<Enviroment, String> invNum;
    public TableColumn<Enviroment, String> serNum;

    public TextField invNumValue;
    public TextField serNumValue;
    public ChoiceBox substationsCB;
    public ChoiceBox envTypesCB;

    public Button backButton;
    public Button updateWrkConfirm;
    public ChoiceBox substationsCBReport;

    @FXML
    TableView<Enviroment> table;

    private static ObservableList<String> sbstForChoiseBox = null;

    private static Enviroment enviromentStatic = null;

    public EnvironmentController() {

    }

    @FXML
    private void initialize() {
        ObservableList<Enviroment> enviromentTypes = new EnvironmentActions().get();

        invNum.setCellValueFactory(new PropertyValueFactory<Enviroment, String>("inventoryNumber"));
        serNum.setCellValueFactory(new PropertyValueFactory<Enviroment, String>("serialNumber"));
        typeCol.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getEnviromentType().getName()));
        cityCol.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getSubstation().getCity().getName()));
        substCol.setCellValueFactory(e -> new SimpleStringProperty(e.getValue().getSubstation().getName()));

        updateWrkConfirm.setDisable(true);
        sbstForChoiseBox = new SubstationActions().getNames();

        substationsCB.setItems(sbstForChoiseBox);
        substationsCB.setTooltip(new Tooltip("Выберите подстанцию"));

        substationsCBReport.setItems(sbstForChoiseBox);
        substationsCBReport.setTooltip(new Tooltip("Выберите подстанцию"));

        envTypesCB.setItems(new EnvironmentTypesActions().getNames());
        envTypesCB.setTooltip(new Tooltip("Выберите тип оборудования"));

        table.setItems(enviromentTypes);
    }

    @FXML
    private void fillTable (ObservableList<Enviroment> enviromentTypes) throws ClassNotFoundException {
        table.setItems(enviromentTypes);
    }

    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<Enviroment> enviroments = new EnvironmentActions().search(search.getText());
            fillTable(enviroments);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска");
        }
    }

    public void delete(ActionEvent actionEvent) {
        Enviroment enviroment  = table.getSelectionModel().getSelectedItem();
        if (enviroment == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new EnvironmentActions().remove(enviroment);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Запись используется в других таблицах");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        Enviroment enviroment  = table.getSelectionModel().getSelectedItem();
        if (enviroment == null){
            resultArea.setText("Выберите запись");
        } else {
            updateWrkConfirm.setDisable(false);
            addBtn.setDisable(true);
            invNumValue.setText(enviroment.getInventoryNumber() + "");
            serNumValue.setText(enviroment.getSerialNumber());
            substationsCB.getSelectionModel().select(getElementPosition(sbstForChoiseBox, enviroment.getSubstation().getName()));
            envTypesCB.getSelectionModel().select(getElementPosition(new EnvironmentTypesActions().getNames(), enviroment.getEnviromentType().getName()));

            enviromentStatic = enviroment;
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if (invNumValue.getText().trim().isEmpty() ||
                serNumValue.getText().trim().isEmpty() ||
                substationsCB.getSelectionModel().getSelectedIndex() == -1 ||
                envTypesCB.getSelectionModel().getSelectedIndex() == -1){
            resultArea.setText("Заполните всю информацию");
        } else {
            try{
                int status = new EnvironmentActions().update(getEnviroment());
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addBtn.setDisable(false);
                    renderTable();
                    updateWrkConfirm.setDisable(true);
                    clearFields();
                } else {
                    resultArea.setText("Ошибка");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                resultArea.setText("Ошибка");
            }
        }
    }

    public void add(ActionEvent actionEvent) {
        if (invNumValue.getText().trim().isEmpty() ||
                serNumValue.getText().trim().isEmpty() ||
                substationsCB.getSelectionModel().getSelectedIndex() == -1 ||
                envTypesCB.getSelectionModel().getSelectedIndex() == -1){
            resultArea.setText("Заполните всю информацию");
        } else {
            try {
                Enviroment enviroment = getEnviroment();
                int status = new EnvironmentActions().add(enviroment);
                if (status == 1) {
                    resultArea.setText("Успешно добавлено");
                    renderTable();
                    clearFields();
                } else {
                    resultArea.setText("Запись не добавлена");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                resultArea.setText("Ошибка");
            }
        }
    }

    @FXML
    private Enviroment getEnviroment(){
        Enviroment enviroment = null;
        try {
            int invNumberValue = Integer.valueOf(invNumValue.getText());
            String serNumberValue = serNumValue.getText();
            String substation = substationsCB.getValue().toString();
            String envType = null;
            try {
               envType = envTypesCB.getValue().toString();
            } catch (Exception e){

            }
            if (envType != null) {
                enviroment = new Enviroment(invNumberValue, serNumberValue, substation, envType);
            } else {
                enviroment = new Enviroment(invNumberValue, serNumberValue, substation);
            }

            if (enviromentStatic != null){
                enviroment.setId(enviromentStatic.getId());
            }
        } catch (NumberFormatException nfe){
            resultArea.setText("введите корректное число");
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        return enviroment;
    }

    private void renderTable(){
        ObservableList<Enviroment> enviroments = new EnvironmentActions().get();
        table.setItems(enviroments);
    }

    private void clearFields(){
        envTypesCB.getSelectionModel().select(-1);
        substationsCB.getSelectionModel().select(-1);
        invNumValue.setText("");
        serNumValue.setText("");
    }



    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }

    public void report(ActionEvent actionEvent) {
        try {
            String substation = substationsCBReport.getValue().toString();
            new EnvironmentReport(substation);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
