package by.gsu.controllers.environment;

import by.gsu.actions.environment.EnvironmentTypesActions;
import by.gsu.controllers.helpers.ConstrollerHelper;
import by.gsu.models.City;
import by.gsu.models.EnviromentType;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

public class EnvTypesController {

    @FXML
    public TextArea resultArea;
    @FXML
    public TextField search;
    public Button addBtn;
    public TextField nameInput;
    public TextField sizeInput;

    public Button backButton;
    public Button updateWrkConfirm;

    @FXML
    TableView<EnviromentType> table;
    @FXML
    private TableColumn<City, String> nameColumn;

    @FXML
    private TableColumn<City, Integer> sizeColumn;

    private static EnviromentType enviromentTypeStatic;

    // инициализируем форму данными
    @FXML
    private void initialize() {
        ObservableList<EnviromentType> enviromentTypes = new EnvironmentTypesActions().get();

        nameColumn.setCellValueFactory(new PropertyValueFactory<City, String>("name"));
        sizeColumn.setCellValueFactory(new PropertyValueFactory<City, Integer>("size"));

        updateWrkConfirm.setDisable(true);

        table.setItems(enviromentTypes);
    }

    @FXML
    private void fillTable (ObservableList<EnviromentType> enviromentTypes) throws ClassNotFoundException {
        table.setItems(enviromentTypes);
    }

    public void search(ActionEvent actionEvent) {
        try {
            ObservableList<EnviromentType> enviromentTypes = new EnvironmentTypesActions().search(search.getText());
            fillTable(enviromentTypes);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            resultArea.setText("Ошибка во время поиска");
        }
    }

    public void delete(ActionEvent actionEvent) {
        EnviromentType enviromentType  = table.getSelectionModel().getSelectedItem();
        if (enviromentType == null){
            resultArea.setText("Выберите запись");
        } else {
            int status = new EnvironmentTypesActions().remove(enviromentType);
            if (status == 1){
                resultArea.setText("Удалено");
                renderTable();
            } else {
                resultArea.setText("Ошибка");
            }
        }
    }

    public void update(ActionEvent actionEvent) {
        EnviromentType enviromentType  = table.getSelectionModel().getSelectedItem();
        if (enviromentType == null){
            resultArea.setText("Выберите запись");
        } else {
            addBtn.setDisable(true);
            updateWrkConfirm.setDisable(false);
            nameInput.setText(enviromentType.getName());
            sizeInput.setText(enviromentType.getSize() + "");
            enviromentTypeStatic = enviromentType;
        }
    }

    public void confirmUpdate(ActionEvent actionEvent) {
        if (!sizeInput.getText().trim().isEmpty() ||
                !nameInput.getText().trim().isEmpty()) {
            try {
                int status = new EnvironmentTypesActions().update(getType());
                if (status == 1) {
                    resultArea.setText("Изменено");
                    addBtn.setDisable(false);
                    renderTable();
                    clearFields();
                    updateWrkConfirm.setDisable(true);
                } else {
                    resultArea.setText("Запись не уникальна");
                }
            } catch (Exception e) {
                resultArea.setText("Ошибка");
            }
        } else {
            resultArea.setText("Заполните все поля для добавления");
        }
    }

    public void add(ActionEvent actionEvent) {
        if (!sizeInput.getText().trim().isEmpty() ||
                !nameInput.getText().trim().isEmpty()){
            try {
                EnviromentType enviromentType = getType();
                int status = new EnvironmentTypesActions().add(enviromentType);
                if (status == 1){
                    resultArea.setText("Успешно добавлено");
                    renderTable();
                    clearFields();
                } else {
                    resultArea.setText("Запись не уникальна");
                }
            }
            catch (Exception e){
                System.out.println(e.getMessage());
                resultArea.setText("Ошибка");
            }
        } else {
            resultArea.setText("Заполните все поля для добавления");
        }

    }

    @FXML
    private EnviromentType getType(){
        EnviromentType enviromentType = null;
        try {
            String name = nameInput.getText();
            int size = Integer.valueOf(sizeInput.getText());
            enviromentType = new EnviromentType(name, size);
            if (enviromentTypeStatic != null){
                enviromentType.setId(enviromentTypeStatic.getId());
            }
        } catch (NumberFormatException nfe){
//            resultArea.setText("введите корректное число");
        }

        return enviromentType;
    }

    private void renderTable(){
        ObservableList<EnviromentType> enviromentTypes = new EnvironmentTypesActions().get();
        table.setItems(enviromentTypes);
    }

    public boolean isValidNumber(String s){
        String regex="([0-9])+";
        return s.matches(regex);
    }

    public void validate(KeyEvent keyEvent) {
        if(isValidNumber(sizeInput.getText().trim())){
            addBtn.setDisable(false);
            if (!updateWrkConfirm.isDisabled()) {
                updateWrkConfirm.setDisable(false);
            }
        }else{
            resultArea.setText("Введите число");
            addBtn.setDisable(true);
            updateWrkConfirm.setDisable(true);
        }
    }

    private void clearFields(){
        sizeInput.setText("");
        nameInput.setText("");
    }

    public void back(ActionEvent actionEvent) {
        new ConstrollerHelper().openMain(actionEvent, backButton);
    }
}
