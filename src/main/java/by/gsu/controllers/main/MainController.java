package by.gsu.controllers.main;

import by.gsu.controllers.helpers.ConstrollerHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class MainController {
    public Button workers;
    ConstrollerHelper constrollerHelper;

    public void workersTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/worker/WorkerView.fxml", "Сотрудники");
    }

    public void citiesTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/city/CityView.fxml", "Населённые пункты");
    }

    public void envTypesTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/environment/EnvTypesView.fxml", "Типы оборудования");
    }

    public void crashTypesTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/crash/CrashTypesView.fxml", "Типы сбое");
    }

    public void substTypesTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/substation/SubStTypesView.fxml", "Типы подстанций");
    }

    public void substationsTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/substation/SubStView.fxml", "Подстанции");
    }

    public void ststTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/statistics/StatisticsView.fxml", "Статистика");
    }

    public void envTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/environment/EnvView.fxml", "Установленное оборудование");
    }

    public void crashTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/crash/CrashView.fxml", "Сбои");
    }

    public void maintenancesTable(ActionEvent actionEvent) {
        openWindow(actionEvent, "/views/maintence/MaintenceView.fxml", "Техническое обслуживание");
    }

    public void openWindow(ActionEvent actionEvent, String viewName, String title){
        Stage primaryStage = new Stage();
        try {
            Parent root = FXMLLoader.load(getClass().getResource(viewName));
            primaryStage.setTitle(title);
            primaryStage.setScene(new Scene(root, 1000, 800));
            primaryStage.show();

            closeFrom();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeFrom(){
        Stage stage = (Stage) workers.getScene().getWindow();
        stage.close();
    }
}
