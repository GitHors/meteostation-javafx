package by.gsu.database;

public class DatabaseConstant {

    //Tables
    public static final String CITY_TABLE_NAME = "Населённые_пункты";
    public static final String CRASH_TYPES_TABLE_NAME = "Типы_сбоев";
    public static final String ENV_TYPES_TABLE_NAME = "Типы_оборудования";
    public static final String SUBSTATION_TYPES_TABLE_NAME = "Типы_подстанций";
    public static final String SUBSTATION_TABLE_NAME = "Подстанции";
    public static final String WORKERS_TABLE_NAME = "Сотрудники";


    // City table: columns names
    public static final String CITY_TABLE_ID_COLUMN = "Код";
    public static final String CITY_TABLE_NAME_COLUMN = "Название_города";
    public static final String CITY_TABLE_CITIZENS_COLUMN = "Численность_населения";
    public static final String CITY_TABLE_REGION_COLUMN = "Регион";


    // Crash types table: columns names
    public static final String CRASH_TYPES_TABLE_ID_COLUMN = "Код";
    public static final String CRASH_TYPES_TABLE_DESCRIPTION_COLUMN = "Описание";


    // Environment types table: columns names
    public static final String ENV_TYPES_TABLE_ID_COLUMN = "Код";
    public static final String ENV_TYPES_TABLE_NAME_COLUMN = "Название";
    public static final String ENV_TYPES_TABLE_SIZE_COLUMN = "Габариты_высота";


    // Substation types table: columns names
    public static final String SUBSTATION_TYPES_TABLE_ID_COLUMN = "Код";
    public static final String SUBSTATION_TYPES_TABLE_MODEL_COLUMN = "Модель";
    public static final String SUBSTATION_TYPES_TABLE_MATERIAL_COLUMN = "Материал_корпуса";


    // Substation table: columns names
    public static final String SUBSTATION_TABLE_ID_COLUMN = "Код";
    public static final String SUBSTATION_TABLE_NAME_COLUMN = "Название";
    public static final String SUBSTATION_TABLE_ID_SUBST_TYPE_COLUMN = "Код_типа_подстанции";
    public static final String SUBSTATION_TABLE_ID_CITY_COLUMN = "Код_населённого_пункта";


    // Workers types table: columns names
    public static final String WORKERS_TABLE_ID_COLUMN = "Код";
    public static final String WORKERS_TABLE_FIO_COLUMN = "ФИО";
    public static final String WORKERS_TABLE_GENDER_COLUMN = "Пол";
    public static final String WORKERS_TABLE_AGE_COLUMN = "Возраст";


    // Statistics table columns names
    public static final String STAT_TABLE_ID_COLUMN = "Код";
    public static final String STAT_TABLE_TEMP_COLUMN = "Температура";
    public static final String STAT_TABLE_PRESSURE_COLUMN = "Давление";
    public static final String STAT_TABLE_WATER_COLUMN = "Количество_осадков";
    public static final String STAT_TABLE_DATE_COLUMN = "Дата";
    public static final String STAT_TABLE_ID_SUBST_COLUMN = "Код_подстанции";
    public static final String STAT_TABLE_AS_COLUMN_CITY = "НазваниеГорода";
    public static final String STAT_TABLE_AS_COLUMN_SUBSTATION = "НазваниеПодстанции";


    // Environment table colomns names
    public static final String ENVIROMNENT_TABLE_ID_COLUMN = "Код";
    public static final String ENVIROMNENT_TABLE_ID_SUBSTATION_COLUMN = "Код_подстанции";
    public static final String ENVIROMNENT_TABLE_ID_ENV_TYPE_COLUMN = "Код_типа_оборудования";
    public static final String ENVIROMNENT_TABLE_INV_NUMBER_COLUMN = "Инвентарный_номер";
    public static final String ENVIROMNENT_TABLE_SERIAL_NUMBER_COLUMN = "Серийный_номер";
    public static final String ENVIROMNENT_TABLE_AS_COLUMN_CITY = "НазваниеГорода";
    public static final String ENVIROMNENT_TABLE_AS_COLUMN_SUBSTATION = "НазваниеПодстанции";
    public static final String ENVIROMNENT_TABLE_AS_COLUMN_ENV_TYPE = "НазваниеТипаОборудования";


    // Crash table colomns names
    public static final String CRASH_TABLE_ID_COLUMN = "Код";
    public static final String CRASH_TABLE_ID_CRASH_TYPE_COLUMN = "Код_сбоя";
    public static final String CRASH_TABLE_ID_ENV_COLUMN = "Код_установленного_оборудования";
    public static final String CRASH_TABLE_DATE_COLUMN = "Дата";
    public static final String CRASH_TABLE_TIME_COLUMN = "Время";
    public static final String CRASH_TABLE_AS_COLUMN_CRASH_TYPE = "ОписаниеСбоя";
    public static final String CRASH_TABLE_AS_COLUMN_ENV_NUMBER = "НомерОборудования";


    // Maintence table colomns names
    public static final String MAINTENCE_TABLE_ID_COLUMN = "Код";
    public static final String MAINTENCE_TABLE_ID_WORKER_COLUMN = "Код_сотрудника";
    public static final String MAINTENCE_TABLE_ID_ENV_COLUMN = "Код_установленного_оборудования";
    public static final String MAINTENCE_TABLE_DATE_COLUMN = "Дата";
    public static final String MAINTENCE_TABLE_AS_COLUMN_WORKER_FOP = "СотрутникОбслуживания";
    public static final String MAINTENCE_TABLE_AS_COLUMN_ENV_NUMBER = "НомерОборудования";


    // Users table colomns names
    public static final String USER_TABLE_ID_COLUMN = "Код";
    public static final String USER_TABLE_LOGIN_COLUMN = "Логин";
    public static final String USER_TABLE_PASSWORD_COLUMN = "Пароль";
    public static final String USER_TABLE_ID_ROLE_COLUMN = "Код_роли";
    public static final String USER_TABLE_AS_ROLE_COLUMN = "Роль";

    // Roles table colomns names
    public static final String ROLE_TABLE_ID_COLUMN = "Код";
    public static final String ROLE_TABLE_ROLE_COLUMN = "Роль";
    public static final String ROLE_TABLE_INFO_COLUMN = "Описание";


    // Stored procedures
    public static final String EXEC_KEY_WORD = "EXEC ";



    // Workers
    public static final String NAME_PROCEDURE_GET_FIO_WORKERS = "ПолучитьФИОСотрудников";
    public static final String NAME_PROCEDURE_SEARCH_WORKERS = "ПоискРаботников";
    public static final String NAME_PROCEDURE_ADD_WORKER = "ДобавитьРаботника";
    public static final String NAME_PROCEDURE_REMOVE_WORKER = "УдалитьСотрудника";
    public static final String NAME_PROCEDURE_UPDATE_WORKER = "ИзменитьСотрудника";

    // Cities
    public static final String NAME_PROCEDURE_SEARCH_CITY = "ПоискНаселённогоПункта";
    public static final String NAME_PROCEDURE_ADD_CITY = "ДобавитьНаселённыйПункт";
    public static final String NAME_PROCEDURE_REMOVE_CITY = "УдалитьНаселённыйПункт";
    public static final String NAME_PROCEDURE_UPDATE_CITY = "ИзменитьНаселённыйПункт";
    public static final String NAME_PROCEDURE_GET_CITIES_NAMES = "ПолучитьНазванияНаселённыхПунктов";


    // Environment types
    public static final String NAME_PROCEDURE_GET_NAMES_ENV_TYPE = "ПолучитьНазваниеТиповОборудования";
    public static final String NAME_PROCEDURE_SEARCH_ENV_TYPE = "ПоискТипаОборудования";
    public static final String NAME_PROCEDURE_ADD_ENV_TYPE = "ДобавитьТипОборудования";
    public static final String NAME_PROCEDURE_REMOVE_ENV_TYPE = "УдалитьТипОборудования";
    public static final String NAME_PROCEDURE_UPDATE_ENV_TYPE = "ИзменитьТипОборудования";


    // Types of crash
    public static final String NAME_PROCEDURE_GET_CRASH_TYPES = "ПолучитьОписаниеТиповСбоев";
    public static final String NAME_PROCEDURE_SEARCH_CRASH_TYPE = "ПоискТипаСбоев";
    public static final String NAME_PROCEDURE_ADD_CRASH_TYPE = "ДобавитьТипСбоя";
    public static final String NAME_PROCEDURE_REMOVE_CRASH_TYPE = "УдалитьТипСбоя";
    public static final String NAME_PROCEDURE_UPDATE_CRASH_TYPE = "ИзменитьТипСбоя";


    // Substation types
    public static final String NAME_PROCEDURE_SEARCH_SUBST_TYPE = "ПоискТипаПодстанции";
    public static final String NAME_PROCEDURE_ADD_SUBST_TYPE = "ДобавитьТипПодстанции";
    public static final String NAME_PROCEDURE_REMOVE_SUBST_TYPE = "УдалитьТипПодстанции";
    public static final String NAME_PROCEDURE_UPDATE_SUBST_TYPE = "ИзменитьТипПодстанции";
    public static final String NAME_PROCEDURE_GET_MODELS_IN_SUBST_TYPES = "ПолучитьНазванияТиповПодстанций";

    // Substations
    public static final String NAME_PROCEDURE_GET_ALL_SUBSTATIONS = "ПолучитьВсеПодстанции";
    public static final String NAME_PROCEDURE_SEARCH_SUBST = "ПоискПодстанции";
    public static final String NAME_PROCEDURE_ADD_SUBST = "ДобавитьПодстанцию";
    public static final String NAME_PROCEDURE_REMOVE_SUBST = "УдалитьПодстанцию";
    public static final String NAME_PROCEDURE_UPDATE_SUBST = "ИзменитьПодстанцию";
    public static final String NAME_PROCEDURE_NAME_SUBST = "ПолучитьНазваниеПодстанций";
    public static final String NAME_PROCEDURE_NAME_SUBST_BY_VALUE = "ПолучитьНазваниеПодстанцийДляГорода";

    // Statistics
    public static final String NAME_PROCEDURE_GET_STAT_BY_CITY = "ПолучитьСтатистикуПоГородуЗаНеделю";
    public static final String NAME_PROCEDURE_GET_STAT = "ПолучитьСтатистику";
    public static final String NAME_PROCEDURE_SEARCH_STAT = "ПоискСтатистики";
    public static final String NAME_PROCEDURE_ADD_STAT = "ДобавитьСтатистику";
    public static final String NAME_PROCEDURE_REMOVE_STAT = "УдалитьСтатистику";
    public static final String NAME_PROCEDURE_UPDATE_STAT = "ИзменитьСтатистику";

    // Environment
    public static final String NAME_PROCEDURE_GET_INV_NUMBERS_ENV = "ПолучитьИнвентарныеНомераОборудования";
    public static final String NAME_PROCEDURE_GET_ENV = "ПолучитьОборудование";
    public static final String NAME_PROCEDURE_SEARCH_ENV = "ПоискУстановленногоОборудования";
    public static final String NAME_PROCEDURE_ADD_ENV = "ДобавитьОборудование";
    public static final String NAME_PROCEDURE_REMOVE_ENV = "УдалитьОборудование";
    public static final String NAME_PROCEDURE_UPDATE_ENV = "ИзменитьОборудование";

    // Crash
    public static final String NAME_PROCEDURE_GET_CRASH = "ПолучитьСбои";
    public static final String NAME_PROCEDURE_SEARCH_CRASH = "ПоискСбоя";
    public static final String NAME_PROCEDURE_ADD_CRASH = "ДобавитьСбой";
    public static final String NAME_PROCEDURE_REMOVE_CRASH = "УдалитьСбой";
    public static final String NAME_PROCEDURE_UPDATE_CRASH = "ИзменитьСбой";

    // Crash
    public static final String NAME_PROCEDURE_GET_MAINTENCE = "ПолучитьОбслуживания";
    public static final String NAME_PROCEDURE_SEARCH_MAINTENCE = "ПоискОбслуживания";
    public static final String NAME_PROCEDURE_ADD_MAINTENCE = "ДобавитьОбслуживание";
    public static final String NAME_PROCEDURE_REMOVE_MAINTENCE = "УдалитьОбслуживание";
    public static final String NAME_PROCEDURE_UPDATE_MAINTENCE = "ИзменитьОбслуживание";

    // users
    public static final String NAME_PROCEDURE_GET_USER = "ПолучитьПользователя";
    public static final String NAME_PROCEDURE_GET_USERS = "ПолучитьПользователей";
    public static final String NAME_PROCEDURE_ADD_USERS = "ДобавитьПользователя";
    public static final String NAME_PROCEDURE_REMOVE_USERS = "УдалитьПользователя";
    public static final String NAME_PROCEDURE_UPDATE_USER_PASSWORD = "ИзменитьПарольПользователя";

    public DatabaseConstant() {
    }
}
