package by.gsu.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public final  class ConnectionProvider {

    private static final String CONNECTION_URL = "jdbc:sqlserver://localhost;integratedSecurity=true;database=meteostation";

    /**
     * Logger.
     *
     * Is used for printing information.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionProvider.class);

    /**
     *
     */
    public ConnectionProvider (){

    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        Connection connection = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection(CONNECTION_URL);

        } catch (SQLException | ClassNotFoundException e) {
            ConnectionProvider.LOGGER.info(e.getMessage());
        }
        return connection;
    }

}
