package by.gsu.actions;

import javafx.collections.ObservableList;

import java.sql.SQLException;

public interface Actions {

    int add(Object object) throws SQLException;
    int remove(Object object);
    int update(Object object);
    ObservableList<?> search(String param);
    ObservableList<?> get();
}
