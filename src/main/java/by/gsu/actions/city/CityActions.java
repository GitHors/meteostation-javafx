package by.gsu.actions.city;

import by.gsu.actions.Actions;
import by.gsu.database.ConnectionProvider;
import by.gsu.database.DatabaseConstant;
import by.gsu.models.City;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static by.gsu.database.DatabaseConstant.*;

public class CityActions implements Actions {

    private static Connection connection = null;

    /**
     * Logger.
     *
     * Is used for printing information.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(CityActions.class);

    public CityActions() {
    }

    @Override
    public int add(Object object) throws SQLException{
        City city = (City)object;
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_CITY +
                            setParameters(3)
            );
            preparedStatement.setString(1, city.getName());
            preparedStatement.setString(2, city.getRegion());
            preparedStatement.setInt(3, city.getAmountOfCitizens());

            status = preparedStatement.executeUpdate();
        } finally {
            closeConnection();
        }

        return status;
    }

    @Override
    public int remove(Object object) {
        City city = (City)object;
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_CITY +
                            setParameters(1)
            );
            preparedStatement.setInt(1, city.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    @Override
    public int update(Object object) {
        City city = (City)object;
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_CITY +
                            setParameters(4)
            );
            preparedStatement.setInt(1, city.getId());
            preparedStatement.setString(2, city.getName());
            preparedStatement.setString(3, city.getRegion());
            preparedStatement.setInt(4, city.getAmountOfCitizens());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    @Override
    public ObservableList<City> search(String param) {
        ObservableList<City> workers = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_CITY +
                            setParameters(1)
            );
            preparedStatement.setString(1, param);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                workers.add(new City(resultSet.getInt(CITY_TABLE_ID_COLUMN),
                        resultSet.getString(CITY_TABLE_NAME_COLUMN),
                        resultSet.getInt(CITY_TABLE_CITIZENS_COLUMN),
                        resultSet.getString(CITY_TABLE_REGION_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return workers;
    }

    /**
     *
     * @return
     */
    public ObservableList<City> get(){
        ObservableList<City> cities = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + DatabaseConstant.CITY_TABLE_NAME);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                cities.add(new City(resultSet.getInt(CITY_TABLE_ID_COLUMN),
                        resultSet.getString(CITY_TABLE_NAME_COLUMN),
                        resultSet.getInt(CITY_TABLE_CITIZENS_COLUMN),
                        resultSet.getString(CITY_TABLE_REGION_COLUMN)));
            }
        } catch (final Exception e) {
            CityActions.LOGGER.info(e.getMessage());
        } finally {
            closeConnection();
        }

        return cities;
    }

    public ObservableList<String> getCitiesNames(){
        ObservableList<String> cities = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(EXEC_KEY_WORD +
                    NAME_PROCEDURE_GET_CITIES_NAMES);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                cities.add(resultSet.getString(CITY_TABLE_NAME_COLUMN));
            }
        } catch (final Exception e) {
            CityActions.LOGGER.info(e.getMessage());
        } finally {
            closeConnection();
        }

        return cities;
    }

    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }


    public static String setParameters(int amount){
        String result = " ";
        for (int i = 0; i < amount - 1; i++){
            result += "?, ";
        }
        result += "?";
        return  result;
    }
}
