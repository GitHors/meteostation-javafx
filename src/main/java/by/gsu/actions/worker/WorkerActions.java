package by.gsu.actions.worker;

import static by.gsu.database.DatabaseConstant.*;

import by.gsu.database.ConnectionProvider;
import by.gsu.models.Worker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class WorkerActions {

    private static Connection connection = null;

    /**
     * Logger.
     *
     * Is used for printing information.
     */
//    private static final Logger LOGGER = LoggerFactory.getLogger(EnvironmentTypesActions.class);

    /**
     *
     * @return
     */
    public ObservableList<Worker> get(){
        ObservableList<Worker> workers = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + WORKERS_TABLE_NAME);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                workers.add(new Worker(resultSet.getInt(WORKERS_TABLE_ID_COLUMN),
                        resultSet.getInt(WORKERS_TABLE_AGE_COLUMN),
                        resultSet.getString(WORKERS_TABLE_FIO_COLUMN),
                        resultSet.getString(WORKERS_TABLE_GENDER_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
            //CityActions.LOGGER.info(e.getMessage());
        } finally {
            closeConnection();
        }

        return workers;
    }

    public ObservableList<String> getWorkers(){
        ObservableList<String> workers = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(EXEC_KEY_WORD + NAME_PROCEDURE_GET_FIO_WORKERS);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                workers.add(resultSet.getString(WORKERS_TABLE_FIO_COLUMN));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return workers;
    }

    public ObservableList<Worker> search(String searchParam){
        ObservableList<Worker> workers = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_WORKERS +
                setParameters(1)
            );
            preparedStatement.setString(1, searchParam);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                workers.add(new Worker(resultSet.getInt(WORKERS_TABLE_ID_COLUMN),
                        resultSet.getInt(WORKERS_TABLE_AGE_COLUMN),
                        resultSet.getString(WORKERS_TABLE_FIO_COLUMN),
                        resultSet.getString(WORKERS_TABLE_GENDER_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return workers;
    }

    public int add(Worker worker) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_WORKER +
                            setParameters(3)
            );
            preparedStatement.setString(1, worker.getFio());
            preparedStatement.setString(2, worker.getGender());
            preparedStatement.setInt(3, worker.getAge());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(Worker worker) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_WORKER +
                            setParameters(4)
            );
            preparedStatement.setInt(1, worker.getId());
            preparedStatement.setString(2, worker.getFio());
            preparedStatement.setString(3, worker.getGender());
            preparedStatement.setInt(4, worker.getAge());

            status = preparedStatement.executeUpdate();
            System.out.println("test");
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (Worker worker){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_WORKER +
                            setParameters(1)
            );
            preparedStatement.setInt(1, worker.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }


    public static String setParameters(int amount){
        String result = " ";
        for (int i = 0; i < amount - 1; i++){
            result += "?, ";
        }
        result += "?";
        return  result;
    }

}
