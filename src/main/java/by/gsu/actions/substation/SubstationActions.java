package by.gsu.actions.substation;
import static by.gsu.database.DatabaseConstant.*;

import by.gsu.database.ConnectionProvider;
import by.gsu.models.Substation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SubstationActions {

    /**
     * Logger.
     *
     * Is used for printing information.
     */
//    private static final Logger LOGGER = LoggerFactory.getLogger(SubstationActions.class);

    private static Connection connection = null;

    public SubstationActions() {
    }

    /**
     *
     * @return
     */
    public ObservableList<Substation> get(){
        ObservableList<Substation> substations = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("EXEC " + NAME_PROCEDURE_GET_ALL_SUBSTATIONS);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                substations.add(new Substation(
                        resultSet.getInt(SUBSTATION_TABLE_ID_COLUMN),
                        resultSet.getString(SUBSTATION_TABLE_NAME_COLUMN),
                        resultSet.getString(CITY_TABLE_NAME_COLUMN),
                        resultSet.getString(SUBSTATION_TYPES_TABLE_MODEL_COLUMN)

                        ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return substations;
    }

    public ObservableList<String> getNames(){
        ObservableList<String> substations = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("EXEC " + NAME_PROCEDURE_NAME_SUBST);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                substations.add(resultSet.getString(SUBSTATION_TABLE_NAME_COLUMN));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return substations;
    }

    public ObservableList<Substation> search(String searchParam){
        ObservableList<Substation> substations = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_SUBST +
                            setParameters(1)
            );
            preparedStatement.setString(1, searchParam);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                substations.add(new Substation(
                        resultSet.getInt(SUBSTATION_TABLE_ID_COLUMN),
                        resultSet.getString(SUBSTATION_TABLE_NAME_COLUMN),
                        resultSet.getString(CITY_TABLE_NAME_COLUMN),
                        resultSet.getString(SUBSTATION_TYPES_TABLE_MODEL_COLUMN)

                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return substations;
    }

    public int add(Substation substation) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_SUBST +
                            setParameters(3)
            );
            preparedStatement.setString(1, substation.getName());
            preparedStatement.setString(2, substation.getCity().getName());
            preparedStatement.setString(3, substation.getSubstationType().getModel());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(Substation substation) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_SUBST +
                            setParameters(4)
            );
            preparedStatement.setInt(1, substation.getId());
            preparedStatement.setString(2, substation.getName());
            preparedStatement.setString(3, substation.getCity().getName());
            preparedStatement.setString(4, substation.getSubstationType().getModel());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (Substation substation){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_SUBST +
                            setParameters(1)
            );
            preparedStatement.setInt(1, substation.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }


    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }


    public static String setParameters(int amount){
        String result = " ";
        for (int i = 0; i < amount - 1; i++){
            result += "?, ";
        }
        result += "?";
        return  result;
    }

}
