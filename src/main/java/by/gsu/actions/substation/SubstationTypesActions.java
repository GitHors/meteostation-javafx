package by.gsu.actions.substation;

import by.gsu.database.ConnectionProvider;
import by.gsu.models.SubstationType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.gsu.database.DatabaseConstant.*;

public class SubstationTypesActions {
    public SubstationTypesActions() {
    }

    private static Connection connection = null;

    /**
     * Logger.
     *
     * Is used for printing information.
     */
//    private static final Logger LOGGER = LoggerFactory.getLogger(SubstationTypesActions.class);


    /**
     *
     * @return
     */
    public ObservableList<SubstationType> get(){
        ObservableList<SubstationType> substationTypes = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + SUBSTATION_TYPES_TABLE_NAME);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                substationTypes.add(new SubstationType(resultSet.getInt(SUBSTATION_TYPES_TABLE_ID_COLUMN),
                        resultSet.getString(SUBSTATION_TYPES_TABLE_MODEL_COLUMN),
                        resultSet.getString(SUBSTATION_TYPES_TABLE_MATERIAL_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return substationTypes;
    }

    public ObservableList<String> getModels(){
        ObservableList<String> substationTypes = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(EXEC_KEY_WORD +
                    NAME_PROCEDURE_GET_MODELS_IN_SUBST_TYPES);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                substationTypes.add(resultSet.getString(SUBSTATION_TYPES_TABLE_MODEL_COLUMN));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return substationTypes;
    }

    public ObservableList<SubstationType> search(String searchParam){
        ObservableList<SubstationType> substationTypes = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_SUBST_TYPE +
                            setParameters(1)
            );
            preparedStatement.setString(1, searchParam);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                substationTypes.add(new SubstationType(
                        resultSet.getInt(SUBSTATION_TYPES_TABLE_ID_COLUMN),
                        resultSet.getString(SUBSTATION_TYPES_TABLE_MODEL_COLUMN),
                        resultSet.getString(SUBSTATION_TYPES_TABLE_MATERIAL_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return substationTypes;
    }

    public int add(SubstationType substationType) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_SUBST_TYPE +
                            setParameters(2)
            );
            preparedStatement.setString(1, substationType.getModel());
            preparedStatement.setString(2, substationType.getMaterial());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(SubstationType substationType) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_SUBST_TYPE +
                            setParameters(3)
            );
            preparedStatement.setInt(1, substationType.getId());
            preparedStatement.setString(2, substationType.getModel());
            preparedStatement.setString(3, substationType.getMaterial());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (SubstationType substationType){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_SUBST_TYPE +
                            setParameters(1)
            );
            preparedStatement.setInt(1, substationType.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }


    public static String setParameters(int amount){
        String result = " ";
        for (int i = 0; i < amount - 1; i++){
            result += "?, ";
        }
        result += "?";
        return  result;
    }
}
