package by.gsu.actions.crash;

import by.gsu.database.ConnectionProvider;
import by.gsu.database.DatabaseConstant;
import by.gsu.models.CrashType;
import by.gsu.models.Worker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.gsu.database.DatabaseConstant.*;

public class CrashTypeActions {

    private static Connection connection = null;

    /**
     * Logger.
     *
     * Is used for printing information.
     */
//    private static final Logger LOGGER = LoggerFactory.getLogger(CrashTypeActions.class);

    public CrashTypeActions() {
    }

    /**
     *
     * @return
     */
    public ObservableList<CrashType> get(){
        ObservableList<CrashType> types = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            // Query for get phone book items through SQL limit
            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + CRASH_TYPES_TABLE_NAME);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                types.add(new CrashType(resultSet.getInt(CITY_TABLE_ID_COLUMN),
                        resultSet.getString(CRASH_TYPES_TABLE_DESCRIPTION_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return types;
    }

    public ObservableList<String> getTypes(){
        ObservableList<String> types = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(EXEC_KEY_WORD + NAME_PROCEDURE_GET_CRASH_TYPES);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                types.add(resultSet.getString(CRASH_TYPES_TABLE_DESCRIPTION_COLUMN));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return types;
    }

    public ObservableList<CrashType> search(String searchParam){
        ObservableList<CrashType> crashTypes = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_CRASH_TYPE +
                            setParameters(1)
            );
            preparedStatement.setString(1, searchParam);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                crashTypes.add(new CrashType(
                        resultSet.getInt(CRASH_TYPES_TABLE_ID_COLUMN),
                        resultSet.getString(CRASH_TYPES_TABLE_DESCRIPTION_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return crashTypes;
    }

    public int add(CrashType crashType) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_CRASH_TYPE +
                            setParameters(1)
            );
            preparedStatement.setString(1, crashType.getDescription());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(CrashType crashType) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_CRASH_TYPE +
                            setParameters(2)
            );
            preparedStatement.setInt(1, crashType.getId());
            preparedStatement.setString(2, crashType.getDescription());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (CrashType crashType){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_CRASH_TYPE +
                            setParameters(1)
            );
            preparedStatement.setInt(1, crashType.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }


    public static String setParameters(int amount){
        String result = " ";
        for (int i = 0; i < amount - 1; i++){
            result += "?, ";
        }
        result += "?";
        return  result;
    }

}
