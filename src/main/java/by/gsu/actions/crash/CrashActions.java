package by.gsu.actions.crash;

import by.gsu.database.ConnectionProvider;
import by.gsu.models.Crash;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.gsu.database.DatabaseConstant.*;
import static by.gsu.actions.helper.ActionsHelper.*;

public class CrashActions {

    public CrashActions() {

    }

    private static Connection connection = null;

    /**
     *
     * @return
     */
    public ObservableList<Crash> get(){
        ObservableList<Crash> crashs = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(EXEC_KEY_WORD + NAME_PROCEDURE_GET_CRASH);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                crashs.add(new Crash(
                        resultSet.getInt(CRASH_TABLE_ID_COLUMN),
                        resultSet.getString(CRASH_TABLE_DATE_COLUMN),
                        resultSet.getString(CRASH_TABLE_TIME_COLUMN),
                        resultSet.getString(CRASH_TABLE_AS_COLUMN_CRASH_TYPE),
                        resultSet.getInt(CRASH_TABLE_AS_COLUMN_ENV_NUMBER)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return crashs;
    }

    public ObservableList<Crash> search(String searchParam){
        ObservableList<Crash> crashs = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_CRASH +
                            setParameters(1)
            );
            preparedStatement.setString(1, searchParam);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                crashs.add(new Crash(
                        resultSet.getInt(CRASH_TABLE_ID_COLUMN),
                        resultSet.getString(CRASH_TABLE_DATE_COLUMN),
                        resultSet.getString(CRASH_TABLE_TIME_COLUMN),
                        resultSet.getString(CRASH_TABLE_AS_COLUMN_CRASH_TYPE),
                        resultSet.getInt(CRASH_TABLE_AS_COLUMN_ENV_NUMBER)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return crashs;
    }

    public int add(Crash crash) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_CRASH +
                            setParameters(4)
            );
            preparedStatement.setString(1, crash.getDate());
            preparedStatement.setString(2, crash.getTime());
            preparedStatement.setString(3, crash.getCrashType().getDescription());
            preparedStatement.setString(4, crash.getEnviroment().getInventoryNumber() + "");

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(Crash crash) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_CRASH +
                            setParameters(5)
            );
            preparedStatement.setInt(1, crash.getId());
            preparedStatement.setString(2, crash.getDate());
            preparedStatement.setString(3, crash.getTime());
            preparedStatement.setString(4, crash.getCrashType().getDescription());
            preparedStatement.setInt(5, crash.getEnviroment().getInventoryNumber());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (Crash crash){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_CRASH +
                            setParameters(1)
            );
            preparedStatement.setInt(1, crash.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
}
