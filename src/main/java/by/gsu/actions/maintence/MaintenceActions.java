package by.gsu.actions.maintence;

import by.gsu.database.ConnectionProvider;
import by.gsu.models.Crash;
import by.gsu.models.Maintenance;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.gsu.actions.helper.ActionsHelper.setParameters;
import static by.gsu.database.DatabaseConstant.*;

public class MaintenceActions {

    public MaintenceActions() {
    }

    private static Connection connection = null;

    /**
     *
     * @return
     */
    public ObservableList<Maintenance> get(){
        ObservableList<Maintenance> maintenances = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(EXEC_KEY_WORD + NAME_PROCEDURE_GET_MAINTENCE);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                maintenances.add(new Maintenance(
                        resultSet.getInt(MAINTENCE_TABLE_ID_COLUMN),
                        resultSet.getString(MAINTENCE_TABLE_DATE_COLUMN),
                        resultSet.getString(MAINTENCE_TABLE_AS_COLUMN_WORKER_FOP),
                        resultSet.getInt(MAINTENCE_TABLE_AS_COLUMN_ENV_NUMBER)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return maintenances;
    }

    public ObservableList<Maintenance> search(String searchParam){
        ObservableList<Maintenance> maintenances = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_MAINTENCE +
                            setParameters(1)
            );
            preparedStatement.setString(1, searchParam);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                maintenances.add(new Maintenance(
                        resultSet.getInt(MAINTENCE_TABLE_ID_COLUMN),
                        resultSet.getString(MAINTENCE_TABLE_DATE_COLUMN),
                        resultSet.getString(MAINTENCE_TABLE_AS_COLUMN_WORKER_FOP),
                        resultSet.getInt(MAINTENCE_TABLE_AS_COLUMN_ENV_NUMBER)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return maintenances;
    }

    public int add(Maintenance maintenance) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_MAINTENCE +
                            setParameters(3)
            );
            preparedStatement.setString(1, maintenance.getDate());
            preparedStatement.setString(2, maintenance.getWorker().getFio());
            preparedStatement.setInt(3, maintenance.getEnviroment().getInventoryNumber());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(Maintenance maintenance) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_MAINTENCE +
                            setParameters(4)
            );
            preparedStatement.setInt(1, maintenance.getId());
            preparedStatement.setString(2, maintenance.getDate());
            preparedStatement.setString(3, maintenance.getWorker().getFio());
            preparedStatement.setInt(4, maintenance.getEnviroment().getInventoryNumber());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (Maintenance maintenance){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_MAINTENCE +
                            setParameters(1)
            );
            preparedStatement.setInt(1, maintenance.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
