package by.gsu.actions.statistics;

import by.gsu.database.ConnectionProvider;
import by.gsu.models.Statistics;
import by.gsu.models.Substation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.gsu.database.DatabaseConstant.*;
import static by.gsu.actions.helper.ActionsHelper.*;

public class StatisticsActions {

    private static Connection connection = null;

    public StatisticsActions() {

    }

    public ObservableList<Statistics> get(){
        ObservableList<Statistics> statistics = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("EXEC " + NAME_PROCEDURE_GET_STAT);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                statistics.add(new Statistics(
                        resultSet.getInt(STAT_TABLE_ID_COLUMN),
                        resultSet.getInt(STAT_TABLE_TEMP_COLUMN),
                        resultSet.getInt(STAT_TABLE_PRESSURE_COLUMN),
                        resultSet.getInt(STAT_TABLE_WATER_COLUMN),
                        resultSet.getString(STAT_TABLE_DATE_COLUMN),
                        resultSet.getString(STAT_TABLE_AS_COLUMN_CITY),
                        resultSet.getString(STAT_TABLE_AS_COLUMN_SUBSTATION)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return statistics;
    }

    public ObservableList<Statistics> get(String city){
        ObservableList<Statistics> statistics = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("EXEC " + NAME_PROCEDURE_GET_STAT_BY_CITY + setParameters(1));

            preparedStatement.setString(1, city);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                statistics.add(new Statistics(
                        resultSet.getInt(STAT_TABLE_ID_COLUMN),
                        resultSet.getInt(STAT_TABLE_TEMP_COLUMN),
                        resultSet.getInt(STAT_TABLE_PRESSURE_COLUMN),
                        resultSet.getInt(STAT_TABLE_WATER_COLUMN),
                        resultSet.getString(STAT_TABLE_DATE_COLUMN),
                        resultSet.getString(STAT_TABLE_AS_COLUMN_CITY),
                        resultSet.getString(STAT_TABLE_AS_COLUMN_SUBSTATION)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return statistics;
    }

    public ObservableList<Statistics> search(String searchParam){
        ObservableList<Statistics> statistics = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_STAT +
                            setParameters(1)
            );
            preparedStatement.setString(1, searchParam);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                statistics.add(new Statistics(
                        resultSet.getInt(STAT_TABLE_ID_COLUMN),
                        resultSet.getInt(STAT_TABLE_TEMP_COLUMN),
                        resultSet.getInt(STAT_TABLE_PRESSURE_COLUMN),
                        resultSet.getInt(STAT_TABLE_WATER_COLUMN),
                        resultSet.getString(STAT_TABLE_DATE_COLUMN),
                        resultSet.getString(STAT_TABLE_AS_COLUMN_CITY),
                        resultSet.getString(STAT_TABLE_AS_COLUMN_SUBSTATION)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return statistics;
    }

    public int add(Statistics statistics) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_STAT +
                            setParameters(5)
            );
            preparedStatement.setString(1, getDateForDB());
            preparedStatement.setInt(2, statistics.getTemperature());
            preparedStatement.setInt(3, statistics.getPressure());
            preparedStatement.setInt(4, statistics.getPrecipitation());
            preparedStatement.setString(5, statistics.getSubstation().getName());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(Statistics statistics) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_STAT +
                            setParameters(5)
            );
            preparedStatement.setInt(1, statistics.getId());
            preparedStatement.setInt(2, statistics.getTemperature());
            preparedStatement.setInt(3, statistics.getPressure());
            preparedStatement.setInt(4, statistics.getPrecipitation());
            preparedStatement.setString(5, statistics.getSubstation().getName());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (Statistics substation){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_STAT +
                            setParameters(1)
            );
            preparedStatement.setInt(1, substation.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }


    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
}
