package by.gsu.actions.user;

import by.gsu.database.ConnectionProvider;
import by.gsu.models.CrashType;
import by.gsu.models.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.gsu.database.DatabaseConstant.*;
import static by.gsu.actions.helper.ActionsHelper.*;

public class UserActions {

    private static Connection connection = null;


    /**
     *
     * @return
     */
    public ObservableList<User> get(){
        ObservableList<User> users = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(EXEC_KEY_WORD + NAME_PROCEDURE_GET_USERS);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getInt(USER_TABLE_ID_COLUMN),
                        resultSet.getString(USER_TABLE_LOGIN_COLUMN),
                        resultSet.getString(USER_TABLE_PASSWORD_COLUMN),
                        resultSet.getString(USER_TABLE_AS_ROLE_COLUMN)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return users;
    }

    public boolean isUserInfoCorrect(User user){
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_GET_USER + setParameters(2));

            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());

            final ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }
        return false;
    }

    public int add(User user) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_USERS +
                            setParameters(2)
            );
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(CrashType crashType) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_CRASH_TYPE +
                            setParameters(2)
            );
            preparedStatement.setInt(1, crashType.getId());
            preparedStatement.setString(2, crashType.getDescription());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (CrashType crashType){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_CRASH_TYPE +
                            setParameters(1)
            );
            preparedStatement.setInt(1, crashType.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
