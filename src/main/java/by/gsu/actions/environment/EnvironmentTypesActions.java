package by.gsu.actions.environment;

import by.gsu.actions.Actions;
import by.gsu.database.ConnectionProvider;
import by.gsu.database.DatabaseConstant;
import by.gsu.models.EnviromentType;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.gsu.database.DatabaseConstant.*;

public class EnvironmentTypesActions implements Actions {

    private static Connection connection = null;

    /**
     * Logger.
     *
     * Is used for printing information.
     */
//    private static final Logger LOGGER = LoggerFactory.getLogger(EnvironmentTypesActions.class);

    public EnvironmentTypesActions() {
    }

    @Override
    public int add(Object object) {
        EnviromentType enviromentType = (EnviromentType) object;
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_ENV_TYPE +
                            setParameters(2)
            );
            preparedStatement.setString(1, enviromentType.getName());
            preparedStatement.setInt(2, enviromentType.getSize());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    @Override
    public int remove(Object object) {
        EnviromentType enviromentType = (EnviromentType) object;
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_ENV_TYPE +
                            setParameters(1)
            );
            preparedStatement.setInt(1, enviromentType.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    @Override
    public int update(Object object) {
        EnviromentType enviromentType = (EnviromentType) object;
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_ENV_TYPE +
                            setParameters(3)
            );
            preparedStatement.setInt(1, enviromentType.getId());
            preparedStatement.setString(2, enviromentType.getName());
            preparedStatement.setInt(3, enviromentType.getSize());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    @Override
    public ObservableList<EnviromentType> search(String param) {
        ObservableList<EnviromentType> enviromentTypes = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_ENV_TYPE +
                            setParameters(1)
            );
            preparedStatement.setString(1, param);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                enviromentTypes.add(new EnviromentType(
                        resultSet.getInt(ENV_TYPES_TABLE_ID_COLUMN),
                        resultSet.getString(ENV_TYPES_TABLE_NAME_COLUMN),
                        resultSet.getInt(ENV_TYPES_TABLE_SIZE_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return enviromentTypes;
    }

    /**
     *
     * @return
     */
    public ObservableList<EnviromentType> get(){
        ObservableList<EnviromentType> enviromentTypes = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            // Query for get phone book items through SQL limit
            final PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + ENV_TYPES_TABLE_NAME);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                enviromentTypes.add(new EnviromentType(
                        resultSet.getInt(ENV_TYPES_TABLE_ID_COLUMN),
                        resultSet.getString(ENV_TYPES_TABLE_NAME_COLUMN),
                        resultSet.getInt(ENV_TYPES_TABLE_SIZE_COLUMN)));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
            //CityActions.LOGGER.info(e.getMessage());
        } finally {
            closeConnection();
        }

        return enviromentTypes;
    }

    public ObservableList<String> getNames(){
        ObservableList<String> enviromentTypes = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            // Query for get phone book items through SQL limit
            final PreparedStatement preparedStatement = connection.prepareStatement(EXEC_KEY_WORD + NAME_PROCEDURE_GET_NAMES_ENV_TYPE);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                enviromentTypes.add(resultSet.getString(DatabaseConstant.ENV_TYPES_TABLE_NAME_COLUMN));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return enviromentTypes;
    }

    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }


    public static String setParameters(int amount){
        String result = " ";
        for (int i = 0; i < amount - 1; i++){
            result += "?, ";
        }
        result += "?";
        return  result;
    }

}
