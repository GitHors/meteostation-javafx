package by.gsu.actions.environment;

import by.gsu.database.ConnectionProvider;
import by.gsu.models.Enviroment;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static by.gsu.actions.helper.ActionsHelper.setParameters;
import static by.gsu.database.DatabaseConstant.*;

public class EnvironmentActions {

    public EnvironmentActions() {
    }

    private static Connection connection = null;

    public ObservableList<Enviroment> get(){
        ObservableList<Enviroment> enviroments = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("EXEC " + NAME_PROCEDURE_GET_ENV);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                enviroments.add(new Enviroment(
                        resultSet.getInt(ENVIROMNENT_TABLE_ID_COLUMN),
                        resultSet.getInt(ENVIROMNENT_TABLE_INV_NUMBER_COLUMN),
                        resultSet.getString(ENVIROMNENT_TABLE_SERIAL_NUMBER_COLUMN),
                        resultSet.getString(ENVIROMNENT_TABLE_AS_COLUMN_ENV_TYPE),
                        resultSet.getString(ENVIROMNENT_TABLE_AS_COLUMN_SUBSTATION),
                        resultSet.getString(ENVIROMNENT_TABLE_AS_COLUMN_CITY)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return enviroments;
    }

    public ObservableList<String> getInventoryNumbers(){
        ObservableList<String> enviroments = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement("EXEC " + NAME_PROCEDURE_GET_INV_NUMBERS_ENV);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                enviroments.add(resultSet.getInt(ENVIROMNENT_TABLE_INV_NUMBER_COLUMN) + "");
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return enviroments;
    }

    public ObservableList<Enviroment> search(String searchParam){
        ObservableList<Enviroment> enviroments = FXCollections.observableArrayList();

        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_SEARCH_ENV +
                            setParameters(1)
            );
            preparedStatement.setString(1, searchParam);

            final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                enviroments.add(new Enviroment(
                        resultSet.getInt(ENVIROMNENT_TABLE_ID_COLUMN),
                        resultSet.getInt(ENVIROMNENT_TABLE_INV_NUMBER_COLUMN),
                        resultSet.getString(ENVIROMNENT_TABLE_SERIAL_NUMBER_COLUMN),
                        resultSet.getString(ENVIROMNENT_TABLE_AS_COLUMN_ENV_TYPE),
                        resultSet.getString(ENVIROMNENT_TABLE_AS_COLUMN_SUBSTATION),
                        resultSet.getString(ENVIROMNENT_TABLE_AS_COLUMN_CITY)
                ));
            }
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return enviroments;
    }

    public int add(Enviroment enviroment) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_ADD_ENV +
                            setParameters(4)
            );
            preparedStatement.setInt(1, enviroment.getInventoryNumber());
            preparedStatement.setString(2, enviroment.getSerialNumber());
            preparedStatement.setString(3, enviroment.getEnviromentType().getName());
            preparedStatement.setString(4, enviroment.getSubstation().getName());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int update(Enviroment enviroment) {
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_UPDATE_ENV +
                            setParameters(4)
            );
            preparedStatement.setInt(1, enviroment.getId());
            preparedStatement.setInt(2, enviroment.getInventoryNumber());
            preparedStatement.setString(3, enviroment.getSerialNumber());
            preparedStatement.setString(4, enviroment.getSubstation().getName());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }

    public int remove (Enviroment enviroment){
        int status = 0;
        try {
            connection = ConnectionProvider.getConnection();

            final PreparedStatement preparedStatement = connection.prepareStatement(
                    EXEC_KEY_WORD + NAME_PROCEDURE_REMOVE_ENV +
                            setParameters(1)
            );
            preparedStatement.setInt(1, enviroment.getId());

            status = preparedStatement.executeUpdate();
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        } finally {
            closeConnection();
        }

        return status;
    }


    private void closeConnection(){
        try {
            if (connection != null){
                connection.close();
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
}
