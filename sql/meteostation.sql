ALTER TABLE ���� DROP CONSTRAINT FK_����_�����_����
ALTER TABLE �������������_������������ DROP CONSTRAINT FK_����_������������_�������������_������������
ALTER TABLE ���� DROP CONSTRAINT FK_�������������_������������_����
ALTER TABLE �������������_������������ DROP CONSTRAINT FK_����������_�������������_������������
ALTER TABLE ���������� DROP CONSTRAINT FK_����������_����������
ALTER TABLE ���_������������ DROP CONSTRAINT FK_�������������_������������_���_������������
ALTER TABLE ���_������������ DROP CONSTRAINT FK_����������_���_������������
ALTER TABLE ���������� DROP CONSTRAINT FK_����_����������_����������
ALTER TABLE ���������� DROP CONSTRAINT FK_���������_������_����������
GO


DROP TABLE ���������_������
GO
CREATE TABLE ���������_������ (
    ��� INT PRIMARY KEY IDENTITY,
    ��������_������ NVARCHAR(50) NOT NUll,
    �����������_��������� INT,
    ������ NVARCHAR(50) NOT NULL	
)
GO


DROP TABLE ����������
GO
CREATE TABLE ���������� (
    ��� INT PRIMARY KEY IDENTITY,
    �������� NVARCHAR(50) NOT NUll,
    ���_����_���������� INT NOT NULL,
    ���_����������_������ INT NOT NULL
)
GO


DROP TABLE ����
GO
CREATE TABLE ���� (
    ��� INT PRIMARY KEY IDENTITY,
    ���_��������������_������������ INT NOT NUll,
    ���� DATE NOT NULL,
    ����� TIME(0) NOT NULL,
    ���_���� INT NOT NULL
)
GO


DROP TABLE ����������
GO
CREATE TABLE ���������� (
    ��� INT PRIMARY KEY IDENTITY,
    ��� NVARCHAR(100) NOT NULL,
    ��� NVARCHAR(30) NOT NULL,
    ������� INT CHECK (������� > 18)
)
GO


DROP TABLE ����������
GO
CREATE TABLE ���������� (
    ��� INT PRIMARY KEY IDENTITY,
    ���� DATE NOT NULL,
    ���_���������� INT NOT NULL,
    ����������� INT NOT NULL,
    �������� INT NOT NULL,
    ����������_������� INT NOT NULL
)
GO


DROP TABLE ���_������������
GO
CREATE TABLE ���_������������ (
    ��� INT PRIMARY KEY IDENTITY,
    ���� DATE NOT NULL,
    ���_���������� INT NOT NULL,
    ���_��������������_������������ INT NOT NULL
) 
GO


DROP TABLE ����_������������
GO
CREATE TABLE ����_������������ (
    ��� INT PRIMARY KEY IDENTITY,
    �������� NVARCHAR(100) UNIQUE NOT NULL,
    ��������_������ INT
)
GO


DROP TABLE ����_����������
GO
CREATE TABLE ����_���������� (
    ��� INT PRIMARY KEY IDENTITY,
    ������ NVARCHAR(50) NOT NULL,
    ��������_������� NVARCHAR(50)
)
GO


DROP TABLE ����_�����
GO
CREATE TABLE ����_����� (
    ��� INT PRIMARY KEY IDENTITY,
    �������� NVARCHAR(150) UNIQUE NOT NULL
)
GO


DROP TABLE �������������_������������
GO
CREATE TABLE �������������_������������ (
    ��� INT PRIMARY KEY IDENTITY,
    ���_���������� INT NOT NULL,
    ���_����_������������ INT NOT NULL,
	�����������_����� INT UNIQUE NOT NULL,
    ��������_����� NVARCHAR(50) NOT NULL
)
GO


DROP TABLE ������������
GO
CREATE TABLE ������������ (
    ��� INT PRIMARY KEY IDENTITY,
    ����� NVARCHAR(50) UNIQUE NOT NULL,
    ������ NVARCHAR(50) NOT NULL,
	���_���� INT DEFAULT 1
)
GO


DROP TABLE ����������������
GO
CREATE TABLE ���������������� (
    ��� INT PRIMARY KEY IDENTITY,
    ���� NVARCHAR(50) UNIQUE NOT NULL,
    �������� NVARCHAR(150) NOT NULL
)
GO


ALTER TABLE ������������
ADD CONSTRAINT FK_����������������_������������
FOREIGN KEY (���_����) REFERENCES ���������������� (���)
GO

ALTER TABLE ����
ADD CONSTRAINT FK_����_�����_����
FOREIGN KEY (���_����) REFERENCES ����_����� (���)
GO

ALTER TABLE �������������_������������
ADD CONSTRAINT FK_����_������������_�������������_������������
FOREIGN KEY (���_����_������������) REFERENCES ����_������������ (���)
GO

ALTER TABLE ����
ADD CONSTRAINT FK_�������������_������������_����
FOREIGN KEY (���_��������������_������������) REFERENCES �������������_������������ (���)
GO

ALTER TABLE �������������_������������
ADD CONSTRAINT FK_����������_�������������_������������
FOREIGN KEY (���_����������) REFERENCES ���������� (���)
GO

ALTER TABLE ����������
ADD CONSTRAINT FK_����������_����������
FOREIGN KEY (���_����������) REFERENCES ���������� (���)
GO

ALTER TABLE ���_������������
ADD CONSTRAINT FK_�������������_������������_���_������������
FOREIGN KEY (���_��������������_������������) REFERENCES �������������_������������ (���)
GO

ALTER TABLE ���_������������
ADD CONSTRAINT FK_����������_���_������������
FOREIGN KEY (���_����������) REFERENCES ���������� (���)
GO

ALTER TABLE ���������� 
ADD CONSTRAINT FK_����_����������_����������
FOREIGN KEY (���_����_����������) REFERENCES ����_���������� (���)
GO

ALTER TABLE ���������� 
ADD CONSTRAINT FK_���������_������_����������
FOREIGN KEY (���_����������_������) REFERENCES ���������_������ (���)
GO

ALTER TABLE ���������� 
ADD CHECK (������� >= 0)
GO

ALTER TABLE ����������
ADD UNIQUE (��������)
GO

INSERT INTO ����_����� VALUES 
    ('���������� �������'),
	('����������� ����')
GO

INSERT INTO ����_������������ VALUES 
    ('��������', 12),
    ('���������', 100)
GO

INSERT INTO ����_���������� VALUES 
    ('�-1', '�����'),
    ('�-2', '�������')
GO

INSERT INTO ���������_������ VALUES 
    ('������', 500000, '���������� �������'),
    ('�����', 1000000, '������� �������'),
    ('�����', 1000000, '��������� �������'),
    ('������', 1000000, '���������� �������'),
    ('������', 1000000, '���������� �������'),
    ('������', 1000000, '���������� �������'),
    ('�������', 1000000, '���������� �������'),
    ('������', 1000000, '����������� �������')
GO

INSERT INTO ���������� VALUES 
    ('������ �.�.', '�������', 19),
    ('����� �.�.', '�������', 19)
GO

INSERT INTO ���������� VALUES 
    ('���-1', 1, 1),
    ('���-2', 1, 1),
    ('���-3', 2, 2)
GO

INSERT INTO ���������� VALUES 
    ('2016-11-09', 1, 11, 756, 100),
    ('2016-09-09', 2, 21, 755, 98),
    ('2016-02-08', 3, 10, 768, 110),
    ('2016-12-01', 1, 15, 748, 20),
    ('2016-11-09', 1, 11, 756, 0),
    ('2016-09-09', 2, 21, 755, 98),
    ('2016-02-08', 3, 19, 768, 110),
	('2017-05-04', 1, 19, 768, 110),
    ('2016-12-01', 1, 15, 748, 0)
GO

INSERT INTO �������������_������������ VALUES 
    (1, 2, 1001, 'QxW234'),
    (2, 1, 1002, 'pQW23F'),
    (3, 2, 1004, 'QW2s34'),
    (1, 1, 1005, 'QW2134'),
    (2, 2, 2003, 'aQW234')
GO

INSERT INTO ���_������������ VALUES 
    ('2014-10-11', 1, 1),
    ('2014-08-12', 2, 5),
    ('2014-03-05', 2, 1),
    ('2015-02-11', 1, 3),
    ('2015-08-12', 2, 4),
    ('2015-07-05', 2, 2)
GO

INSERT INTO ���� VALUES 
    (1, '2016-01-03', '12:04:00', 2),
    (2, '2016-05-18','1:20:00', 1),
    (2, '2016-06-08','11:50:00', 1),
    (1, '2016-02-05', '11:04:00', 2),
    (2, '2016-03-11','2:50:00', 1),
    (2, '2016-02-10','16:50:00', 1),
    (2, '2016-02-11','16:55:00', 2)
GO

INSERT INTO ���������������� VALUES 
    ('user','user info')
GO

INSERT INTO ������������(�����, ������) VALUES 
    ('test','test')
GO


/*
*/
DROP PROC ���������������������
GO
CREATE PROC ��������������������� 
AS 
    SELECT * FROM ����������, ����_����������, ���������_������
    WHERE  ����������.���_����_���������� = ����_����������.��� AND
		����������.���_����������_������ = ���������_������.���
GO


/*
�������� ��������� ��� �������� � ������� ���������
*/


/*
�����
*/
DROP PROC ����������������������
GO
CREATE PROC ����������������������
AS
	SELECT ��� FROM ���������� ORDER BY ��� 
GO


DROP PROC ���������������
GO
CREATE PROC ��������������� (@search VARCHAR(40))
AS
	SELECT * FROM ���������� 
	    WHERE ����������.��� LIKE '%' + @search + '%' OR 
		      ����������.��� LIKE '%' + @search + '%' OR 
			  CAST (����������.������� AS varchar) = @search
GO


DROP PROC �����������������
GO
CREATE PROC ����������������� (@fio NVARCHAR(100), @gender NVARCHAR(30), @age INT)
AS
	INSERT INTO ���������� VALUES (@fio, @gender, @age)
GO


DROP PROC �����������������
GO
CREATE PROC ����������������� (@id INT)
AS
    DELETE FROM ���������� WHERE ��� = @id
GO


DROP PROC ������������������
GO
CREATE PROC ������������������ (@id INT, @fio NVARCHAR(100), @gender NVARCHAR(30), @age INT)
AS
    UPDATE ���������� SET ��� = @fio, ��� = @gender, ������� = @age WHERE ��� = @id
GO



/*
������ � �������� ��������� ������
*/

DROP PROC ��������������������������������
GO
CREATE PROC ��������������������������������
AS
	SELECT ��������_������ FROM ���������_������ ORDER BY ������, ��������_������
GO

DROP PROC ���������������������
GO
CREATE PROC ��������������������� (@search VARCHAR(40))
AS
	SELECT * FROM ���������_������ 
	    WHERE ��������_������ LIKE '%' + @search + '%' OR 
		      ������ LIKE '%' + @search + '%' OR 
			  CAST (�����������_��������� AS varchar) = @search
GO


DROP PROC ����������������������
GO
CREATE PROC ���������������������� (@city_name NVARCHAR(50), @region NVARCHAR(50), @amount_of_peopple INT)
AS
	INSERT INTO ���������_������ VALUES (@city_name, @amount_of_peopple, @region)
GO


DROP PROC ���������������������
GO
CREATE PROC ��������������������� (@id INT)
AS
    DELETE FROM ���������_������ WHERE ��� = @id
GO



DROP PROC ����������������������
GO
CREATE PROC ���������������������� (@id INT, @city_name NVARCHAR(50), @region NVARCHAR(50), @amount_of_peopple INT)
AS
    UPDATE ���������_������ SET ��������_������ = @city_name, ������ = @region, �����������_��������� = @amount_of_peopple WHERE ��� = @id
GO



/*
�������� ��� ����� ������������
*/
DROP PROC ���������������������������������
GO
CREATE PROC ���������������������������������
AS
	SELECT �������� FROM ����_������������ 
GO


DROP PROC ���������������������
GO
CREATE PROC ��������������������� (@search VARCHAR(40))
AS
	SELECT * FROM ����_������������ 
	    WHERE �������� LIKE '%' + @search + '%' OR 
			  CAST (��������_������ AS varchar) = @search
GO


DROP PROC �����������������������
GO
CREATE PROC ����������������������� (@name NVARCHAR(100), @size INT)
AS
	INSERT INTO ����_������������ VALUES (@name, @size)
GO


DROP PROC ����������������������
GO
CREATE PROC ���������������������� (@id INT)
AS
    DELETE FROM ����_������������  WHERE ��� = @id
GO


DROP PROC �����������������������
GO
CREATE PROC ����������������������� (@id INT, @name NVARCHAR(100), @size INT)
AS
    UPDATE ����_������������ SET �������� = @name, ��������_������ = @size WHERE ��� = @id
GO



/*
������ � ������ �����
*/
DROP PROC ��������������������������
GO
CREATE PROC ��������������������������
AS
	SELECT �������� FROM ����_����� ORDER BY ��������
GO


DROP PROC ��������������
GO
CREATE PROC �������������� (@search VARCHAR(40))
AS
	SELECT * FROM ����_����� 
	    WHERE �������� LIKE '%' + @search + '%'
GO

DROP PROC ���������������
GO
CREATE PROC ��������������� (@name NVARCHAR(150))
AS
	INSERT INTO ����_����� VALUES (@name)
GO


DROP PROC ��������������
GO
CREATE PROC �������������� (@id INT)
AS
    DELETE FROM ����_����� WHERE ��� = @id
GO


DROP PROC ���������������
GO
CREATE PROC ��������������� (@id INT, @name NVARCHAR(150))
AS
    UPDATE ����_����� SET �������� = @name WHERE ��� = @id
GO



/*
������ � �������� ���� ����������.
*/
DROP PROC �������������������������������
GO
CREATE PROC �������������������������������
AS
	SELECT ������ FROM ����_���������� ORDER BY ������
GO


DROP PROC �������������������
GO
CREATE PROC ������������������� (@search VARCHAR(40))
AS
	SELECT * FROM ����_����������
	    WHERE ������ LIKE '%' + @search + '%' OR
		��������_������� LIKE '%' + @search + '%'
GO


DROP PROC ���������������������
GO
CREATE PROC ��������������������� (@model NVARCHAR(50), @material NVARCHAR(50))
AS
	INSERT INTO ����_���������� VALUES (@model, @material)
GO


DROP PROC ��������������������
GO
CREATE PROC �������������������� (@id INT)
AS
    DELETE FROM ����_���������� WHERE ��� = @id
GO


DROP PROC ���������������������
GO
CREATE PROC ��������������������� (@id INT, @model NVARCHAR(50), @material NVARCHAR(50))
AS
    UPDATE ����_���������� SET ������ = @model, ��������_������� = @material WHERE ��� = @id
GO



/*
������ � ������������
*/
DROP PROC ��������������������������
GO
CREATE PROC ��������������������������
AS
	SELECT �������� 
	    FROM ���������� 
		ORDER BY ��������
GO


DROP PROC �����������������������������������
GO
CREATE PROC ����������������������������������� (@city_name NVARCHAR(40))
AS
	SELECT �������� 
	    FROM ����������
		WHERE ����������.���_����������_������ = (SELECT ��� FROM ���������_������ WHERE ��������_������ = @city_name)
		ORDER BY ��������
GO


DROP VIEW ��������������������������
GO
CREATE VIEW ��������������������������
AS
    SELECT ����������.*, ��������_������, ������ 
	    FROM ����������, ���������_������, ����_����������
	    WHERE ����������.���_����������_������ = ���������_������.��� 
		     AND ����������.���_����_���������� = ����_����������.���
GO


DROP PROC ���������������
GO
CREATE PROC ��������������� (@search VARCHAR(40))
AS
	SELECT * FROM ��������������������������
	    WHERE  �������� LIKE '%' + @search + '%' OR
		       ��������_������ LIKE '%' + @search + '%' OR
		       ������ LIKE '%' + @search + '%'
GO


DROP PROC ������������������
GO
CREATE PROC ������������������ (@nazv NVARCHAR(50), @name_city NVARCHAR(50), @name_type NVARCHAR(50))
AS
	DECLARE @id_city INT;
	DECLARE @id_type INT;
	
	SET @id_city = (SELECT ��� FROM ���������_������ WHERE ��������_������ = @name_city);
	SET @id_type = (SELECT ��� FROM ����_���������� WHERE ������ = @name_type);
	
	INSERT INTO ���������� VALUES (@nazv, @id_city, @id_type)
GO


DROP PROC �����������������
GO
CREATE PROC ����������������� (@id INT)
AS
    DELETE FROM ���������� WHERE ��� = @id
GO


DROP PROC ������������������
GO
CREATE PROC ������������������ (@id INT, @nazv NVARCHAR(50), @name_city NVARCHAR(50), @name_type NVARCHAR(150))
AS
	DECLARE @id_city INT;
	DECLARE @id_type INT;
	
	SET @id_city = (SELECT ��� FROM ���������_������ WHERE ��������_������ = @name_city);
	SET @id_type = (SELECT ��� FROM ����_���������� WHERE ������ = @name_type);

    UPDATE ���������� SET �������� = @nazv, ���_����������_������ = @id_city, ���_����_���������� = @id_type WHERE ��� = @id
GO



/*
������ � �������� ����������
*/
DROP VIEW ��������������������������
GO
CREATE VIEW ��������������������������
AS
    SELECT ����������.*, ����������.�������� as ������������������, ���������_������.��������_������ as ��������������
	    FROM ����������, ����������, ���������_������
	    WHERE ����������.���_���������� = ����������.��� AND 
		      ����������.���_����������_������ = ���������_������.���
GO


DROP PROC ������������������
GO
CREATE PROC ������������������
AS
	SELECT * FROM ��������������������������
GO


DROP PROC ����������������������������������
GO
CREATE PROC ���������������������������������� (@city VARCHAR(40))
AS
    SELECT * 
	    FROM ��������������������������
	    WHERE �������������� = @city
			  AND ���� >= (GETDATE() - 7)
		ORDER BY ����
GO


DROP PROC ���������������
GO
CREATE PROC ��������������� (@search VARCHAR(40))
AS
	SELECT *
	    FROM ��������������������������
	    WHERE CAST (���� as nvarchar) =  @search OR
			CAST (����������� as nvarchar) =  @search OR
			CAST (�������� as nvarchar) =  @search OR
			CAST (����������_������� as nvarchar) =  @search OR
			������������������ LIKE '%' + @search + '%'
GO


DROP PROC ������������������
GO
CREATE PROC ������������������ (@date DATE, @temp INT, @pressure INT, @osadki INT, @name_substation NVARCHAR(50))
AS
	DECLARE @id_substation INT;
	
	SET @id_substation = (SELECT ��� FROM ���������� WHERE �������� = @name_substation);

	INSERT INTO ���������� VALUES (@date, @id_substation, @temp, @pressure, @osadki)
GO


DROP PROC �����������������
GO
CREATE PROC ����������������� (@id INT)
AS
    DELETE FROM ���������� WHERE ��� = @id
GO


DROP PROC ������������������
GO
CREATE PROC ������������������ (@id INT, @temp INT, @pressure INT, @osadki INT, @name_substation NVARCHAR(50))
AS
	DECLARE @id_substation INT;
	SET @id_substation = (SELECT ��� FROM ���������� WHERE �������� = @name_substation);
	
    UPDATE ���������� SET ���_���������� = @id_substation, 
	    ����������� = @temp, �������� = @pressure, ����������_������� = @osadki WHERE ��� = @id
GO


DROP FUNCTION ��������������������������
GO
CREATE FUNCTION �������������������������� (@city_name VARCHAR(40)) RETURNS TABLE
AS
	RETURN (SELECT ROUND(AVG(CAST(����������� as FLOAT)), 2) as ������������������,
	               ROUND(AVG(CAST(�������� as FLOAT)), 2) as ���������������,
				   ROUND(AVG(CAST(����������_������� as FLOAT)), 2) as �������������������,
			       ������������������ 
			FROM ��������������������������
			GROUP BY ������������������)
GO



/*
�������� ��� �������� ������������� ������������
*/
DROP VIEW ���������������������������������
GO
CREATE VIEW ��������������������������������� AS
	SELECT �������������_������������.*, 
	       ����������.�������� as ������������������, 
		   ���������_������.��������_������ as ��������������, 
		   ����_������������.�������� as ������������������������
	    FROM �������������_������������, ����������, ���������_������, ����_������������
	    WHERE �������������_������������.���_����_������������ = ����_������������.��� AND 
		      �������������_������������.���_���������� = ����������.��� AND
			  ����������.���_����������_������ = ���������_������.���
GO


DROP PROC ��������������������
GO
CREATE PROC ��������������������
AS
	SELECT * FROM ���������������������������������
GO


DROP PROC �������������������������������������
GO
CREATE PROC �������������������������������������
AS
	SELECT �����������_����� FROM �������������_������������ ORDER BY �����������_�����
GO


DROP PROC �������������������������������
GO
CREATE PROC ������������������������������� (@search VARCHAR(40))
AS
	SELECT *
	    FROM ���������������������������������
	    WHERE  
		    ������������������ LIKE '%' + @search + '%' OR
			�������������� LIKE '%' + @search + '%' OR
			������������������������ LIKE '%' + @search + '%' OR
			��������_����� LIKE '%' + @search + '%' OR
			�����������_����� LIKE '%' + @search + '%'
GO


DROP PROC ��������������������
GO
CREATE PROC �������������������� (@inventory_number INT, @serial_number NVARCHAR(50), @name_environment_type NVARCHAR(50), @name_substation NVARCHAR(50))
AS
	DECLARE @id_environment_type INT;
	DECLARE @id_substation INT;
	
    SET @id_environment_type = (SELECT ��� FROM ����_������������ WHERE �������� = @name_environment_type);
	SET @id_substation = (SELECT ��� FROM ���������� WHERE �������� = @name_substation);

	INSERT INTO �������������_������������ VALUES (@id_substation, @id_environment_type, @inventory_number, @serial_number)
GO


DROP PROC �������������������
GO
CREATE PROC ������������������� (@id INT)
AS
    DELETE FROM �������������_������������ WHERE ��� = @id
GO


DROP PROC ��������������������
GO
CREATE PROC �������������������� (@id INT, @inventory_number INT, @serial_number NVARCHAR(50), @name_substation NVARCHAR(50))
AS
	DECLARE @id_substation INT;
	SET @id_substation = (SELECT ��� FROM ���������� WHERE �������� = @name_substation);
	
    UPDATE �������������_������������ SET ���_���������� = @id_substation, �����������_����� = @inventory_number, ��������_����� = @serial_number WHERE ��� = @id
GO


DROP FUNCTION �����������������������������
GO
/*
    ������� ������������� ��� �������� ���������� ����� �� ������������� ������������.

	@���_��������������_������������ (��� �������������� ������������ ��� �������� ��������� ���������� �����),
	return int - ���������� �����.
*/
CREATE FUNCTION ����������������������������� (@���_������������ int) RETURNS INT
AS
BEGIN
    DECLARE @res INT
	SET @res = (SELECT count(*) FROM �������������_������������ 
	                RIGHT JOIN ���� ON �������������_������������.��� = ����.���_��������������_������������
	                     WHERE �������������_������������.��� = @���_������������)
	RETURN @res
END
GO


DROP FUNCTION ��������������������������������������
GO
CREATE FUNCTION �������������������������������������� (@substation_name VARCHAR(40)) RETURNS TABLE
AS
	RETURN (SELECT �����������_�����, ��������_�����, ��������, dbo.�����������������������������(�������������_������������.���) as ����
	            FROM �������������_������������, ����_������������ 
				WHERE �������������_������������.���_����_������������ = ����_������������.��� AND 
				���_���������� = (SELECT ��� FROM ���������� WHERE �������� = @substation_name))
GO


/*
������ � �������� ����
*/

DROP VIEW ������������������������
GO
CREATE VIEW ������������������������ AS
	SELECT ����.*, 
	       ����_�����.�������� as ������������, 
		   �������������_������������.�����������_����� as ����������������� 
	    FROM ����, ����_�����, �������������_������������
	    WHERE ����.���_���� = ����_�����.��� AND
		����.���_��������������_������������ = �������������_������������.���
GO


DROP PROC ������������
GO
CREATE PROC ������������
AS
	SELECT * FROM ������������������������
GO


DROP PROC ���������
GO
CREATE PROC ��������� (@search VARCHAR(40))
AS
	SELECT *
	    FROM ������������������������
	    WHERE  
		    ���� = @search OR
			����� = @search OR
			����������������� LIKE '%' + @search + '%' OR
			������������ LIKE '%' + @search + '%'
GO


DROP PROC ������������
GO
CREATE PROC ������������ (@date_param DATE, @time_param TIME, @crash_description NVARCHAR(150), @inv_number INT)
AS
	DECLARE @id_crash_type INT;
	DECLARE @id_environment INT;
	
    SET @id_crash_type = (SELECT ��� FROM ����_����� WHERE �������� = @crash_description);
	SET @id_environment = (SELECT ��� FROM �������������_������������ WHERE �����������_����� = @inv_number);

	INSERT INTO ���� VALUES (@id_environment, @date_param, @time_param, @id_crash_type)
GO


DROP PROC �����������
GO
CREATE PROC ����������� (@id INT)
AS
    DELETE FROM ���� WHERE ��� = @id
GO


DROP PROC ������������
GO
CREATE PROC ������������ (@id INT, @date_param DATE, @time_param TIME, @crash_description NVARCHAR(150), @inv_number INT)
AS
	DECLARE @id_crash_type INT;
	DECLARE @id_environment INT;
	
    SET @id_crash_type = (SELECT ��� FROM ����_����� WHERE �������� = @crash_description);
	SET @id_environment = (SELECT ��� FROM �������������_������������ WHERE �����������_����� = @inv_number);
	
    UPDATE ���� SET ���� = @date_param, ����� = @time_param, ���_���� = @id_crash_type, ���_��������������_������������ = @id_environment WHERE ��� = @id
GO



/*
�������� ��� �������� ����������� ������������
*/

DROP VIEW ���������������������������������
GO
CREATE VIEW ��������������������������������� AS
	SELECT ���_������������.*, 
	       ����������.��� as ���������������������, 
		   �������������_������������.�����������_����� as �����������������
	    FROM ���_������������, ����������, �������������_������������
	    WHERE ���_������������.���_���������� = ����������.��� AND 
		���_������������.���_��������������_������������ = �������������_������������.���
GO


DROP PROC ��������������������
GO
CREATE PROC ��������������������
AS
	SELECT * FROM ���������������������������������
GO


DROP PROC �����������������
GO
CREATE PROC ����������������� (@search VARCHAR(40))
AS
	SELECT *
	    FROM ���������������������������������
	    WHERE  
		    ���� = @search OR
			��������������������� LIKE '%' + @search + '%' OR
			����������������� = @search
GO


DROP PROC ��������������������
GO
CREATE PROC �������������������� (@date_param DATE, @worker_fio NVARCHAR(150), @inv_number INT)
AS
	DECLARE @id_worker INT;
	DECLARE @id_environment INT;
	
    SET @id_worker = (SELECT ��� FROM ���������� WHERE ��� = @worker_fio);
	SET @id_environment = (SELECT ��� FROM �������������_������������ WHERE �����������_����� = @inv_number);

	INSERT INTO ���_������������ VALUES (@date_param, @id_worker, @id_environment)
GO


DROP PROC �������������������
GO
CREATE PROC ������������������� (@id INT)
AS
    DELETE FROM ���_������������ WHERE ��� = @id
GO


DROP PROC ��������������������
GO
CREATE PROC �������������������� (@id INT, @date_param DATE, @worker_fio NVARCHAR(150), @inv_number INT)
AS
	DECLARE @id_worker INT;
	DECLARE @id_environment INT;
	
    SET @id_worker = (SELECT ��� FROM ���������� WHERE ��� = @worker_fio);
	SET @id_environment = (SELECT ��� FROM �������������_������������ WHERE �����������_����� = @inv_number);
	
    UPDATE ���_������������ SET ���� = @date_param, ���_���������� = @id_worker, ���_��������������_������������ = @id_environment WHERE ��� = @id
GO



/*
�������� ��� �������� ������������
*/

DROP VIEW ��������������������������������
GO
CREATE VIEW �������������������������������� AS
	SELECT ������������.*, ����������������.���� as ����
	    FROM ������������, ����������������
	    WHERE ������������.���_���� = ����������������.���
GO


DROP PROC ���������������������
GO
CREATE PROC ���������������������
AS
	SELECT * FROM ��������������������������������
GO


DROP PROC ��������������������
GO
CREATE PROC �������������������� (@login NVARCHAR(50), @password NVARCHAR(50))
AS
	INSERT INTO ������������ (�����, ������) VALUES (@login, @password)
GO


DROP PROC ��������������������
GO
CREATE PROC �������������������� (@login NVARCHAR(50), @password NVARCHAR(50))
AS
	SELECT * FROM ������������ WHERE ����� = @login AND ������ = @password
GO


DROP PROC �������������������
GO
CREATE PROC ������������������� (@id INT)
AS
    DELETE FROM ������������ WHERE ��� = @id
GO


DROP PROC ��������������������������
GO
CREATE PROC �������������������������� (@id INT, @password NVARCHAR(150))
AS
    UPDATE ������������ SET ������ = @password WHERE ��� = @id
GO